#pragma once

World *init_world(const int env_height, const int env_width, const float cell_size, std::vector<std::vector<int>>& world_matrix);

bool load_world(std::string filename, std::vector<std::vector<int>>& world_matrix, float& env_x1, float& env_y1);
