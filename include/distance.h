#pragma once
#include <World.h>

float euc_dist_to_center(const float center_x, const float center_y, Cell* cell);

float man_dist_to_center(const float center_x, const float center_y, Cell* cell);

float euc_dist(Cell* cell1, Cell* cell2);

float man_dist(Cell* cell1, Cell* cell2);

float dist_from_tree(Spanning_tree& tree, Cell* cell);

float distance_from_unexplored(Cell* cell, World* world);
