#pragma once
#include <unordered_map>
#include <vector>
#include "World.h"

int uniform_fill(int i, int j, std::vector<std::vector<int>>& world);

bool  approximate_world(std::vector<std::vector<int>>& old_world, std::vector<std::vector<int>>& new_world);

void build_horizontal_tree(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle);

void build_vertical_tree(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle);

void cycle_to_trajectory(mrs_msgs::TrajectoryReference& cmd, std::unordered_map<std::string, std::unordered_set<int>>& cycle, Cell* start, float edge_size, float cell_size, float env_x1, float env_y1, float z, float speed);

void explore_CG(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle, std::unordered_map<std::string, std::unordered_set<int>>& priority_edges, std::vector<std::tuple<int, int, int>>& additional_edges, bool horizontal);

void cycle_to_trajectory_GC(mrs_msgs::TrajectoryReference& cmd, std::unordered_map<std::string, std::unordered_set<int>>& cycle, std::unordered_map<std::string, std::unordered_set<int>>& priority_edges, Cell* start, float edge_size, float cell_size, float env_x1, float env_y1, float z, float speed);
