#pragma once

#include <vector>
#include <unordered_set>
#include "Cell.h"

class World
// A class which represents the world
{
  public:
    int height;
    int width;
    float cell_size;
    float center_oi_x;
    float center_oi_y;
    std::vector<std::vector<Cell*>> cells;
    std::unordered_set<Cell*> unexplored_cells;
  // constructor
  World(int n_c_rows=5, int n_c_cols=5, float c_size = 0.5);

  // destructor
  ~World();

  void create_world_cells(std::vector<std::vector<int>>& world_matrix);

  void visualise(Cell* current_pos);
  // a method to visualize the world

  void erase_from_unexplored(Cell* cell);
};

