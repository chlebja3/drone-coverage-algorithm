#pragma once
#include "Spanning_tree.h"
#include "World.h"

int cell_status_to_int(Cell_status status);

void make_edge(Cell* cell1, Cell* cell2);

float euc_dist_numbers(int x1, int y1, int x2, int y2);

int man_dist_numbers(int x1, int y1, int x2, int y2);

float calculate_cell_score(const float center_x, const float center_y, Cell* target_cell, std::vector<Spanning_tree>& other_trees, Spanning_tree& tree, World* world);

Cell* nearest_cell_from_tree(Spanning_tree& tree, Cell* cell);

std::vector<Cell*> get_candidate_cells(Spanning_tree& tree, World* world);

int count_neighbours_in_ST(Cell* cell, Cell*& next_cell, int st_num, World* world );

bool can_unmark(Cell* cell, int st_num, World* world);

void pick_cell(Spanning_tree& spanning_tree, World* world, std::vector<Spanning_tree>& other_spanning_trees, std::vector<Cell*>& redundant_cells, Cell*& current_pos);
