#pragma once

#include <vector>
#include <unordered_set>
enum Cell_status{UNEXPLORED,EXPLORED, OBSTACLE};


class Cell
// A class which represents one cell in the map of the environment
{
  public:
  int x; // the x coordinate of the cell
  int y; // the y coordinate of the cell
  int steps_from_start; // for the spanning tree -> trajectory conversion
  std::unordered_set<int> spanning_trees; //the spanning trees this cell belongs to
  bool visited;

  Cell_status cell_status;
  
  // constructor
  Cell(int X, int Y, Cell_status status);

  private:
  // edges
  std::vector<Cell*> edges;

  public:
  void add_edge(Cell* cell);
  void remove_edge(Cell* cell);
  bool in_spanning_tree(int id);
  Cell* go_to_next_cell(int start_x, int start_y);
  void print_edges();
  bool all_edges_visited();
  void count_steps_from_start();
  void erase_edges();
};

