#pragma once

#include <vector>
#include "Cell.h"

class Spanning_tree
// A class which represents one spanning tree 
{
  public:
    std::vector<Cell*> cells;
  private:
    int id;

  public:
  int get_id()
  {
    return id;
  }

  Spanning_tree(Cell* start, int ID)
  {
    id = ID; 
    cells.push_back(start);
  }
};

