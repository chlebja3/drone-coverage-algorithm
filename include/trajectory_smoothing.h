#pragma once
#include <mrs_msgs/TrajectoryReference.h>
#include <Eigen/Dense>

void smooth_trajectory(mrs_msgs::TrajectoryReference& cmd,float z=3, int p=5, float cell_size=1, float u=0.2);
