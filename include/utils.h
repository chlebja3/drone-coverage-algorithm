#pragma once
#include "drone_coverage_planner/pos_change.h"

int my_round(const float x);

int name_to_ID(std::string name);

std::string ID_to_name(int ID);

bool check_validity(float x, float y,float env_x1,float env_y1,float env_x2,float env_y2);

drone_coverage_planner::pos_change fill_pos_change_msg(int start_x, int start_y, std::string uav_name, int id, int explored=0);
