#include <string>
#include "drone_coverage_planner/pos_change.h"

bool check_validity(float x, float y,float env_x1,float env_y1,float env_x2,float env_y2)
// check that given [x,y] coordinates are inside environment bounds
{
  if (x < env_x1 || y < env_y1 || x >= env_x2 || y >= env_y2)
  {
    return false;
  }
  return true;
}

int name_to_ID(std::string name)
{
  return (int) std::strtol(&name[name.find('v') + 1], nullptr, 10); // this will fork only if name is uavX where X is the ID
}

std::string ID_to_name(int ID)
{
  return "uav" + std::to_string(ID);
}

int my_round(const float x)
// rounding function which rounds only up
{
	if (x - (int)x > 0)
	{
		return (int)x+1;
	}
	return (int)x;
}

drone_coverage_planner::pos_change fill_pos_change_msg(int start_x, int start_y, std::string uav_name, int id, int explored)
{
  drone_coverage_planner::pos_change message;
  message.x = start_x;
  message.y = start_y;
  message.explored = explored;
  message.uav_name = uav_name;
  message.id = id;
  return message;
}
