#include <ros/ros.h>
#include <iostream>
#include <fstream>

#include "World.h"
#include "utils.h"

World *init_world(const int env_height, const int env_width, const float cell_size, std::vector<std::vector<int>>& world_matrix)
{

  /*
   * calculate the number of cells in one row
   * and in one column
   */

  World* w = new World(env_width, env_height, cell_size);

  // calculate the center of inertia
  w->center_oi_y = (float) (w->height - 1) / 2;
  w->center_oi_x = (float) (w->width - 1) / 2; 

  ROS_INFO("Center of inertia is [%f, %f].",
      w->center_oi_x, w->center_oi_y);

  ROS_INFO("Environment representation created with %dx%d cells.",
     w->width, w->height);

  w->create_world_cells(world_matrix);
  ROS_INFO("Created world cells.");

  return w;
}

bool load_world(std::string filename, std::vector<std::vector<int>>& world_matrix, float& env_x1, float& env_y1)
{
  // open the file
  std::ifstream f(filename);
  if (f.fail()) // check that the file was opened successfully
  {
    std::cout << "Unable to open world description file." << std::endl;
    return false;
  }

  // get the dimensions of the world and env_x1, env_y1, env_x2, env_y2
  int rows;
  int cols;
  f >> rows >> cols;
  std::cout << "Number of rows: " << rows <<" and number of columns: " << cols << std::endl;
  if (rows <= 0 || cols <= 0)
  {
    std::cout << "Invalid world dimensions." << std::endl;
    return false;
  }

  f >> env_x1 >> env_y1; 
  std::cout << "Environment start: [" << env_x1 <<", " << env_y1 << "]" << std::endl;

  // fill the world representation matrix
  for (int i = 0; i < rows; ++i)
  {
    std::vector<int> current_row;
    for (int j = 0; j < cols; ++j)
    {
      int value;
      f >> value;
      //if (value > 0) std::cout << "SOmething bigger than 0." << std::endl;
      current_row.push_back(value);
    }
    world_matrix.push_back(current_row);
  }

  // close the file descriptor
  f.close();
  
  // indicate success
  return true;
}
