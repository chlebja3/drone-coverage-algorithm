#include <algorithm>
#include <limits>
#include <ros/ros.h>
#include <mutex>
#include <sstream>
#include <vector>
#include <math.h>
#include <mrs_msgs/UavState.h>
#include <mrs_msgs/TrajectoryReference.h>
#include <std_srvs/Trigger.h>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <tf2/LinearMath/Quaternion.h>

#include <mrs_lib/param_loader.h>

#include "mrs_msgs/TrajectoryReferenceSrvResponse.h"
#include "mrs_msgs/VelocityReferenceStamped.h"
#include "mrs_msgs/ObstacleSectors.h"
#include "ros/duration.h"
#include "ros/forwards.h"
#include "ros/node_handle.h"
#include "ros/steady_timer_options.h"
#include "ros/transport_hints.h"

#include "Cell.h"
#include "cell_functions.h"
#include "World.h"
#include "Spanning_tree.h"
#include "distance.h"
#include "utils.h"
#include "world_functions.h"
#include "spanning_tree_algo.h"
#include "trajectory_smoothing.h"
#include "obstacle_detection.h"

// custom msgs and services
#include "drone_coverage_planner/pos_change.h"
#include "drone_coverage_planner/Explore.h"
#include "drone_coverage_planner/SetAlgorithm.h"

#define REDUNDANCY_REDUCTION_ITERATIONS 3
#define OBSTACLE_TRIGGER_DIST 2 

void pos_change_callback(drone_coverage_planner::pos_change const &msg);

void bumper_callback(const mrs_msgs::ObstacleSectors_<std::allocator<void>>::ConstPtr & msg);

void odom_callback(mrs_msgs::UavState::ConstPtr const &msg);

void wait_for_message(int check_frequency, ros::Publisher& pub, drone_coverage_planner::pos_change& message);

bool set_algo(drone_coverage_planner::SetAlgorithm::Request &req, drone_coverage_planner::SetAlgorithm::Response &res);

bool set_active(drone_coverage_planner::Explore::Request &req, drone_coverage_planner::Explore::Response &res);

void receive_pos_change(std::map<std::string, int>& name_to_index, std::vector<Cell*>& redundant_cells, std::vector<Spanning_tree>& other_spanning_trees, World* world, bool& end, bool& my_turn, std::string& prev_uav, drone_coverage_planner::pos_change &msg);

float get_heading();

void obstacle_callback(drone_coverage_planner::pos_change const &msg);

bool check_obstacles(float &obs_x, float &obs_y, float env_x1, float env_y1);

void increment_highest_id(drone_coverage_planner::pos_change &message);

void create_publishers(std::vector<std::string>& names, ros::NodeHandle& n);

void broadcast(drone_coverage_planner::pos_change& message);

void broadcast_obstacle(drone_coverage_planner::pos_change& message);

void create_publishers_pos_change(std::vector<std::string>& names, ros::NodeHandle& n);

void create_publishers_obstacles(std::vector<std::string>& names, ros::NodeHandle& n);

// a global variable to store messages
std::vector<drone_coverage_planner::pos_change> received;
mrs_msgs::ObstacleSectors::ConstPtr bumper_msg = NULL;
mrs_msgs::UavState::ConstPtr uav_odom = NULL;
std::mutex                   uav_odom_mutex;
World* global_world = NULL;
bool reset = false;
bool use_lidar = false;
std::vector<ros::Publisher> pos_change_publishers;
std::vector<ros::Publisher> obstacle_publishers;
std::vector<ros::Subscriber> subscribers;
std::vector<ros::Subscriber> subscribers_obstacles;

// a global variable to store the drone name
std::string uav_name;

// TODO dynamic parameters??
float flight_height = 2;
int smoothing_point_num = 5;
float smoothing_u = 0.15;
float cell_size = 1; 
float speed = 2;

int highest_id = 0;
std::unordered_set<std::string> received_ids = {};

int obstacle_ready = 0;

bool spanning_tree_algo = true; // a global variable, to decide which algorithm should be used
bool active = false; // indicaton if the node is active

int main(int argc, char **argv)
{
  // initialize ros
	ros::init(argc, argv, "coverage_planner");
  //ROS_INFO("Coverage planner initialized.");

  // create node hadler
	ros::NodeHandle n;

  // initialize parameters
  int start_x;
  int start_y;
  int drone_num ;
  float env_x1;
  float env_y1;
  float env_x2;
  float env_y2;
  std::string world_file_name;
  std::string algorithm;
  std::string node_name = ros::this_node::getName();
  std::vector<std::string> names_of_uavs;
  std::cout << "The name of this node is " << node_name << std::endl;
  
  // load parameters
  mrs_lib::ParamLoader param_loader(n, "DroneCoverage");
  param_loader.loadParam(node_name + "/uav_name", uav_name);
  param_loader.loadParam(node_name + "/start_x", start_x);
  param_loader.loadParam(node_name + "/start_y", start_y);
  param_loader.loadParam("drone_num", drone_num);
  param_loader.loadParam("world_file_path", world_file_name);
  param_loader.loadParam("algorithm", algorithm);
  param_loader.loadParam("flight_height", flight_height);
  param_loader.loadParam("smoothing_point_num", smoothing_point_num);
  param_loader.loadParam("smoothing_u", smoothing_u);
  param_loader.loadParam("cell_size", cell_size);
  param_loader.loadParam("use_lidar", use_lidar);
  param_loader.loadParam("drone_names", names_of_uavs);

  if (!param_loader.loadedSuccessfully())
  {
    ROS_ERROR("Parameter loading failure");
    return 1;
  }
  for (auto &n: names_of_uavs) std::cout << "Uav name is " << n << std::endl;
  //std::cout << "Uav name is " << uav_name << "." << std::endl; 
  //std::cout << "Starting position is [" << start_x << ", " << start_y << "]." << std::endl; 
  //std::cout << "Environment is rectangle [" << env_x1 << ", " << env_y1 << "], [" << env_x2 << ", " << env_y2 << "]." << std::endl;
  

  // initialize the service servers
  std::string set_algo_srv_name = "/" + uav_name + "/SetAlgorithm";
  ros::ServiceServer algo_settig_srv = n.advertiseService(set_algo_srv_name, set_algo);
  std::string explore_srv_name = "/" + uav_name + "/Explore";
  ros::ServiceServer explore_srv = n.advertiseService(explore_srv_name, set_active);
  
  ROS_INFO("Node initialized. Waiting for the activation service.");


  if (algorithm == "CG") 
  {
    ROS_INFO("Default algorithm is 'Cycle Growing Algorithm'");
    spanning_tree_algo = false;
  }
  else if (algorithm == "ST")
  {
    ROS_INFO("Default algorithm is 'Spanning Tree Coverage Algorithm'");
    spanning_tree_algo = true;
  }
  else
  {
    ROS_ERROR("Unknown algorithm parameter. Please choose between 'ST' and 'CG' algorithms.");
    return EXIT_FAILURE;
  }

  ROS_INFO("Parameters loaded successfully.");

  while (true)
  {
    ros::spinOnce();
    ros::Duration(0.1).sleep();

    if (active) break;
  }
  ROS_INFO("Node acivated.");

  // load world representation matrix
  std::vector<std::vector<int>> world_matrix;
  if (!load_world(world_file_name, world_matrix, env_x1, env_y1))
  {
    ROS_ERROR("Unable to load world representation matrix.");
    return EXIT_FAILURE;
  }
  ROS_INFO("World representation matrix loaded successfully.");

  // divide the environment to cells

  std::vector<std::vector<int>> new_world;
  if (spanning_tree_algo && approximate_world(world_matrix, new_world))
  {
    ROS_INFO("The world can be approximated by merging cells.");
    world_matrix = new_world;
    start_x /= 2;
    start_y /= 2;
    cell_size *= 2;
  }
  else
  {
    ROS_INFO("The world can not be approximated.");
  }

  int env_width = world_matrix[0].size();
  int env_height = world_matrix.size();

  ROS_INFO("Environment has width %d and height %d.", env_width, env_height);

  env_x2 = env_x1 +  (float)cell_size*((float)env_width);
  env_y2 = env_y1 + (float) cell_size*((float)env_height);

  ROS_INFO("Environment starts at [%f, %f] and ends at [%f, %f].", env_x1, env_y1, env_x2, env_y2);
  if (check_validity(env_x1 + (float) start_x*cell_size, env_y1 + (float) start_y*cell_size, env_x1, env_y1, env_x2, env_y2) == false)
  {
    ROS_ERROR("Invalid starting point.");
    return EXIT_FAILURE;
  }

  ROS_INFO("Start position in virtual world is [%d, %d], the gps coordinates of start are [%.5f, %.5f]", start_x, start_y, env_x1 + (float)start_x, env_y1 + (float) start_y);	

  // create world - dont forget to free
  World *world = init_world(env_height, env_width, cell_size, world_matrix);
  ROS_INFO("World initialized.");

  if (world->cells[start_y][start_x]->cell_status == Cell_status::OBSTACLE)
  {
    ROS_ERROR("Starting point can not be obstacle.");
    return EXIT_FAILURE;
  }
  

  Cell* current_pos = world->cells[start_y][start_x]; 
  current_pos->cell_status = Cell_status::EXPLORED;
  
  world->visualise(current_pos);
  global_world = world;

  // Initialize spanning tree
  Spanning_tree spanning_tree(current_pos, name_to_ID(uav_name));
  current_pos->spanning_trees.insert(spanning_tree.get_id());
  world->erase_from_unexplored(current_pos);
  ROS_INFO("Spanning tree initialized.");

  create_publishers_pos_change(names_of_uavs, n);

  // fill the first message
  drone_coverage_planner::pos_change message = fill_pos_change_msg(start_x, start_y, uav_name, highest_id);

  //send the first message
  ROS_INFO("Publishing the first message.");
  
  ROS_INFO("Going to sleep");
  while((int) received.size() < drone_num - 1)
  {
    broadcast(message);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
    //ROS_INFO("Spinned Once, received has size %d", (int) received.size());
  }
  std::string prev_uav;
  
  std::vector<std::string> uav_names;
  std::map<std::string, int> name_to_index;
  std::map<int, int> uav_num_to_index;

  std::vector<Spanning_tree> other_spanning_trees;
  std::unordered_set<Cell*> starting_points;

  for (int i=0; i <drone_num -1 ; ++i)
  {
    Cell* new_cell = world->cells[received[i].y][received[i].x];
    starting_points.insert(new_cell);
    std::string uav = received[i].uav_name;
    uav_names.push_back(uav);
    Spanning_tree new_tree(new_cell, name_to_ID(uav)); 
    ROS_INFO("Created a tree with id %d.", new_tree.get_id());
    other_spanning_trees.push_back(new_tree);
    new_cell->cell_status = Cell_status::EXPLORED;
    new_cell->spanning_trees.insert(name_to_ID(uav));
    world->erase_from_unexplored(new_cell);
    name_to_index[uav] = i;
    uav_num_to_index[name_to_ID(uav)] = i;
  }
  for (int i=0; i < drone_num -1; ++i)
  {
    received.erase(received.begin());
  }
  ROS_INFO("Received messages after erase %ld", received.size());
  
  int my_idx = name_to_ID(uav_name);
  int prev_ID = my_idx;
  int highest_idx = my_idx;
  // find the previous uav
  for (int i = 0; i < uav_names.size(); ++i)
  {
    int idx = name_to_ID(uav_names[i]);
    if (idx < my_idx)
    {
      if (prev_ID == my_idx) prev_ID = idx;
      else
      {
        if (prev_ID < idx) prev_ID = idx;
      }
    }
    if (idx > highest_idx) highest_idx = idx;
  }

  // now take care of the overflow

  bool my_turn = false;
  if (prev_ID == my_idx) // this is the uav with smallest ID
  {
    prev_ID = highest_idx;

    my_turn = true;

  }
  prev_uav = ID_to_name(prev_ID);
  std::cout << "Previous uav is " << prev_uav <<"." << std::endl;
  std::vector<Cell*> redundant_cells;
  bool finished = false;
  ros::Duration(0.5).sleep();

  while (!finished)
  {
  // grow the tree
  while (world->unexplored_cells.size() > 0)
  {
    bool end = false;
    while (my_turn != true)
    {
      ROS_INFO_THROTTLE(5, "Waiting for my turn.");
      broadcast(message);
      receive_pos_change(name_to_index, redundant_cells, other_spanning_trees, world, end, my_turn, prev_uav, message);
    }
    if (end)
    {
      break;
    }
    if (drone_num > 1)
    {
      my_turn = false;
    }
    ROS_INFO("Choosing candidate cells.");

    pick_cell(spanning_tree, world, other_spanning_trees, redundant_cells, current_pos);

    // publish position change
    increment_highest_id(message);
    message = fill_pos_change_msg(current_pos->x, current_pos->y, uav_name, highest_id);
    broadcast(message);
    
    //ROS_INFO("Pos change [%d, %d] published.", current_pos->x, current_pos->y);
  }
  ROS_INFO("World explored");
  for (int i = 0; i < 30; ++i) // to fix another sunchronization thing
  {
    broadcast(message);
    ros::Duration(0.1).sleep();
  }
  ROS_INFO("Erasing redundant cells.");

  // Now get rid of the redundant cells
  for (int i = 0; i < REDUNDANCY_REDUCTION_ITERATIONS; ++i)
  {
    std::unordered_map<Cell*, std::vector<int>> to_erase;
    for (int j = 0; j < redundant_cells.size(); ++j)
    {
      Cell* cell = redundant_cells[j];
      for (int uav_num: redundant_cells[j]->spanning_trees)
      {
        Spanning_tree* cur_spanning_tree;
        if (uav_num == my_idx) cur_spanning_tree = &spanning_tree;
        else cur_spanning_tree = &other_spanning_trees[uav_num_to_index[uav_num]];
        while (true)
        {
          if (cell == NULL || cell->spanning_trees.size() <= 1 || cell->cell_status == Cell_status::UNEXPLORED)
          {
            break;
          }
          Cell* next_cell;
          int neigh = count_neighbours_in_ST(cell, next_cell, uav_num, world);
          std::vector<Cell*>::iterator pos = std::find(cur_spanning_tree->cells.begin(), cur_spanning_tree->cells.end(), cell);
          if (std::find(starting_points.begin(), starting_points.end(), cell) == starting_points.end() && pos != cur_spanning_tree->cells.end() && (neigh <= 1 || (neigh == 3 && can_unmark(cell, uav_num, world))))
          {
            cur_spanning_tree->cells.erase(pos);
            if (to_erase.find(cell) != to_erase.end()) to_erase[cell].push_back(uav_num);
            else to_erase[cell] = {uav_num};
            //cell->spanning_trees.erase(std::find(cell->spanning_trees.begin(), cell->spanning_trees.end(), uav_num));
            std::cout << "Erasing cell " << cell->x << ", " << cell->y << " from tree " << cur_spanning_tree->get_id() << std::endl;
            cell = next_cell;
          }
          else break;
        }
        cell = redundant_cells[j]; // reset cell
      }
    }
    for (auto &it: to_erase)
    {
      for (int uav_name: it.second)
      {
        it.first->spanning_trees.erase(std::find(it.first->spanning_trees.begin(), it.first->spanning_trees.end(), uav_name));
      }
      if (it.first->spanning_trees.size() == 0)
      {
        ROS_INFO("Found unexplored cell.");
        it.first->cell_status = Cell_status::UNEXPLORED;
        world->unexplored_cells.insert(it.first);
      }
    }
    while (world->unexplored_cells.size() > 0)
    {
      broadcast(message);
      ros::spinOnce();
      int shortest_uav = my_idx;
      int len = spanning_tree.cells.size();
      for (int j = 0; j < other_spanning_trees.size(); ++j)
      {
        int new_len = other_spanning_trees[j].cells.size();
        if (new_len < len)
        {
          len = new_len;
          shortest_uav = other_spanning_trees[j].get_id();
        }
        else if (new_len == len) shortest_uav = std::min(shortest_uav, other_spanning_trees[j].get_id());
      }
      //std::cout << "Uav with shortest tree is " << shortest_uav << std::endl;
      // expand tree of the chosen uav
      if (my_idx == shortest_uav)
      {
      ROS_INFO("Reducing redundancy.");
      pick_cell(spanning_tree, world, other_spanning_trees, redundant_cells, current_pos);

      // publish position change
      increment_highest_id(message);
      message = fill_pos_change_msg(current_pos->x, current_pos->y, uav_name, highest_id);
      for (int k = 0; k < 20; ++k)
      {
        broadcast(message);
        ros::Duration(0.05).sleep();
      }
      }
      else // receive pos change and update world representation
      {
        bool end = false; // not needed
        bool my_turn = false; // not needed
        receive_pos_change(name_to_index, redundant_cells, other_spanning_trees, world, end, my_turn, prev_uav, message);
        if (end) break;
      }
    }
  }
  for (int i = 0; i < 30; ++i) // to fix another sunchronization thing
  {
    broadcast(message);
    ros::Duration(0.1).sleep();
  }


  // algorithm 2
  // decide if the tree is horizontal or vertical
  bool horizontal = false;
  std::unordered_set<int> x_coords;
  std::unordered_set<int> y_coords;
  for (auto it: spanning_tree.cells)
  {
    x_coords.insert(it->x);
    y_coords.insert(it->y);
  }
  if (x_coords.size() >= y_coords.size())
  {
    horizontal = true;
  }

  std::cout << "Spanning tree is " << ((horizontal == true)?"horizontal":"vertical") << std::endl;

  //the edge reorganization
  ROS_INFO("Starting edge reorganization.");
  int st_ID = spanning_tree.get_id();
  std::unordered_map<std::string, std::unordered_set<int>> cycle;
  std::unordered_map<std::string, std::unordered_set<int>> priority_edges;
  std::vector<std::tuple<int, int, int>> additional_edges;

  // generate the spanning tree
  float edge_size = 0.5;
  if (!spanning_tree_algo)
  {
    edge_size = 1;
    ROS_INFO("Building trajectory using cycle growing algorithm.");
    explore_CG(world, st_ID, cycle, priority_edges, additional_edges, horizontal); 
  }
  else if (horizontal)
  {
    ROS_INFO("Building trajectory using spanning tree algorithm horizontal version.");
    build_horizontal_tree(world, st_ID, cycle);
  }
  else //vertical
  {
    ROS_INFO("Building trajectory using spanning tree algorithm vertical version.");
    build_vertical_tree(world, st_ID, cycle);
  }

  ROS_INFO("Successfully generated the cycle.");
  // convert the generated cycle to trajectory

  mrs_msgs::TrajectoryReference cmd;
  if (!spanning_tree_algo)
  {
    cycle_to_trajectory_GC(cmd, cycle, priority_edges, world->cells[start_y][start_x], edge_size, cell_size, env_x1, env_y1, flight_height, speed);
  }
  else
  {
    cycle_to_trajectory(cmd, cycle,world->cells[start_y][start_x], edge_size, cell_size, env_x1, env_y1, flight_height, speed);
  }
  ROS_INFO("Generated cycle converted to trajectory.");

  // smooth the trajectory
  ROS_INFO("Started trajectory smoothing.");
  smooth_trajectory(cmd, flight_height, smoothing_point_num, cell_size, smoothing_u);
  ROS_INFO("Trajectory smoothing finished.");

  // fly along the generated trajectory

	std::string traj_ref_pub_topic  = uav_name + "/control_manager/trajectory_reference";
	std::string odom_sub_topic = uav_name + "/odometry/uav_state";
  
	ros::Subscriber odom_sub  = n.subscribe(odom_sub_topic, 10, odom_callback);
	ros::Publisher  traj_pub   = n.advertise<mrs_msgs::TrajectoryReference>(traj_ref_pub_topic, 10, true);

  ROS_INFO("Publisher created.");

  ros::ServiceClient client = n.serviceClient<std_srvs::Trigger>(uav_name + "/control_manager/start_trajectory_tracking");
  ros::ServiceClient client_stop = n.serviceClient<std_srvs::Trigger>(uav_name + "/control_manager/stop_trajectory_tracking");
  ros::ServiceClient client_resume = n.serviceClient<std_srvs::Trigger>(uav_name + "/control_manager/resume_trajectory_tracking");
  
  ros::ServiceClient client_goto_start = n.serviceClient<std_srvs::Trigger>(uav_name + "/control_manager/goto_trajectory_start");

  ROS_INFO("Service clients created.");
  std_srvs::Trigger srv;
  
  // give ROS some time
	ros::Rate rate(100);
  while (traj_pub.getNumSubscribers() == 0)
  {
    rate.sleep();
  }

  ROS_INFO("Publishing reference trajectory message.");
  traj_pub.publish(cmd);

  ros::Duration(0.5).sleep();

  ROS_INFO("Going to start of the trajectory.");
  
  if (client_goto_start.call(srv))
  { 
     ROS_INFO("Started flying to the start of the reference trajectory.");
  }
  else
  {
    ROS_ERROR("Failed to call service.");
  }
  
  Cell pos_cell(-1, -1, Cell_status::UNEXPLORED);
  current_pos = &pos_cell;
  ROS_INFO("Created position representation.");
  
  // mark all cells as unexplored
  for (int i = 0; i < world->height; ++i)
  {
    for (int j = 0; j < world->width; ++j)
    {
      Cell* c = world->cells[i][j];
      c->erase_edges();
      if (c->cell_status != Cell_status::OBSTACLE && c->spanning_trees.size() > 0)
      {
        c->cell_status = Cell_status::UNEXPLORED;
        world->unexplored_cells.insert(c);
        c->spanning_trees.clear();
      }
    }
  }

  ROS_INFO("Exploration status of cells reseted.");

  int k = 0;
  while(uav_odom == NULL)
  {
    ros::spinOnce();
    ros::Duration(0.1).sleep(); // wait for odometry
    if (k++ == 100) return EXIT_FAILURE;
  }
  
  ROS_INFO("Odometry is being received");

  // Detect that the starting point was reached
  k = 0;
  while (current_pos->x != start_x || current_pos->y != start_y)
  {
    if (k++ == 100) break; // prevent neverending while cycle
    ros::spinOnce();
    update_map_position(current_pos, env_x1, env_y1, env_x2, env_y2, cell_size, uav_odom->pose.position.x, uav_odom->pose.position.y);
    //ROS_INFO("Odometry [%f, %f], position in map [%d, %d].", uav_odom->pose.position.x, uav_odom->pose.position.y, current_pos->x, current_pos->y);
    ros::Duration(0.5).sleep(); // wait for odometry
  }
  ROS_INFO("Starting point reached.");

  // mark the starting position as explored and signal to other drones that it is explored
  world->cells[current_pos->y][current_pos->x]->cell_status = Cell_status::EXPLORED;
  //world->cells[current_pos->y][current_pos->x]->spanning_trees.insert(spanning_tree.get_id());
  world->erase_from_unexplored(world->cells[current_pos->y][current_pos->x]);
  increment_highest_id(message);
  message.x = current_pos->x;
  message.y = current_pos->y;
  message.explored = Cell_status::EXPLORED;
  message.uav_name = uav_name;
  message.id = highest_id;
  broadcast(message);
  ROS_INFO("Signaled that start was reached.");
  
  if (use_lidar)
  {
  // start detecting obstacles
	std::string bumper_topic  = uav_name + "/bumper/obstacle_sectors";
  ros::Subscriber bumper_sub = n.subscribe(bumper_topic, 10, bumper_callback);

  ROS_INFO("Subscribed to the bumper topic.");

  // wait for bumper data
  k = 0;
  while(bumper_msg == NULL)
  {
    ros::spinOnce();
    ros::Duration(0.1).sleep(); // wait for odometry
    if (k++ == 50)
    {
      ROS_ERROR("Bumper data not arriving. Exiting.");
      if (client.call(srv)) ROS_INFO("Trajectory tracking started.");
      else
      {
        ROS_ERROR("Failed to call service.");
        return EXIT_FAILURE;
      }
      return EXIT_SUCCESS;
    }
  }

	ros::Rate bump_rate(10);
  int counter = 0;
  bool counting = false;

  // init the obstacle signalization topic
  subscribers_obstacles.clear();
  create_publishers_obstacles(names_of_uavs, n);

  ROS_INFO("Waiting for subscribers");
  int more = 0;
  obstacle_ready = 0;
  drone_coverage_planner::pos_change msg;
  while (obstacle_ready < drone_num || more++ < 10)
  {
    ROS_INFO_THROTTLE(5,"Only %d drones are ready.", obstacle_ready);
    msg.x = -1;
    msg.y = -1;
    msg.uav_name = uav_name;
    broadcast_obstacle(msg);
    ros::Duration(0.1).sleep();
    ros::spinOnce();
  }

  //start tracking the trajectory
  if (client.call(srv)) ROS_INFO("Trajectory tracking started.");
  else
  {
    ROS_ERROR("Failed to call service.");
    return EXIT_FAILURE;
  }

  std::map<int, Cell*> tree_starting_points;
  while (true)
  {
    broadcast_obstacle(msg);
    ros::spinOnce();
    bump_rate.sleep();
    //ROS_INFO("Bumper data, %f, threshold %f.", bumper_msg->sectors[0], cell_size*(OBSTACLE_TRIGGER_DIST + 0.5));
    if(bumper_msg->sectors[0] > -0.1 && bumper_msg->sectors[0] < cell_size*(OBSTACLE_TRIGGER_DIST + 0.5))
    {
      if (counting)
      {
        ++counter;
        if (counter > 20)
        {
          ROS_WARN("Static obstacle detected.");
          float cur_heading = get_heading();
          int obstacle_x = (uav_odom->pose.position.x + cos(cur_heading)*bumper_msg->sectors[0] - env_x1)/cell_size;
          int obstacle_y = (uav_odom->pose.position.y + sin(cur_heading)*bumper_msg->sectors[0] - env_y1)/cell_size;
          ROS_INFO("Current heading is %f, obstacle is [%d, %d]", cur_heading, obstacle_x, obstacle_y);
          // signal the obstacle to others
          drone_coverage_planner::pos_change message_obst;
          message_obst.x = obstacle_x;
          message_obst.y = obstacle_y;
          message_obst.explored = Cell_status::OBSTACLE;
          message_obst.uav_name = uav_name;
          message_obst.id = highest_id;
          broadcast_obstacle(message_obst);
        }
      }
      else
      {
        ROS_WARN("Obstacle right in front of uav detected.\n");
        float cur_heading = get_heading();
        int obstacle_x = (uav_odom->pose.position.x + cos(cur_heading)*bumper_msg->sectors[0] - env_x1)/cell_size;
        int obstacle_y = (uav_odom->pose.position.y + sin(cur_heading)*bumper_msg->sectors[0] - env_y1)/cell_size;

        ROS_INFO("Current heading is %f, obstacle is [%d, %d]", cur_heading, obstacle_x, obstacle_y);
        if (obstacle_x >= 0 && obstacle_y >= 0 && obstacle_x < world->width && obstacle_y < world->height && world->cells[obstacle_y][obstacle_x]->cell_status != Cell_status::OBSTACLE)
        {
          counting = true;
          if (client_stop.call(srv)) ROS_INFO("Trajectory tracking stoped.");
          else ROS_ERROR("Failed to call service.");
          ++counter;
        }
        else ROS_INFO("Obstacle already in the obsatcle map or outside.");
      }
    }
    else
    {
      if (counting)
      {
        counter = 0;
        counting = false;
        client_resume.call(srv);
        ROS_INFO("Resuming trajectory tracking.");
      }
    }

    if (update_map_position(current_pos, env_x1, env_y1, env_x2, env_y2, cell_size, uav_odom->pose.position.x, uav_odom->pose.position.y))
      // signalize that new cell was explored and mark as obstacle
    {
      world->cells[current_pos->y][current_pos->x]->cell_status = Cell_status::EXPLORED;
      //world->cells[current_pos->y][current_pos->x]->spanning_trees.insert(spanning_tree.get_id());
      world->erase_from_unexplored(world->cells[current_pos->y][current_pos->x]);
      ROS_INFO("Visited [%d,%d]", current_pos->x, current_pos->y);
      increment_highest_id(message);
      message.x = current_pos->x;
      message.y = current_pos->y;
      message.id = highest_id;
      message.explored = Cell_status::EXPLORED;
      message.uav_name = uav_name;
      broadcast(message);
    }

    if (reset)
    {
      if (client_stop.call(srv)) ROS_INFO("Trajectory tracking stoped.");
      reset = false;
      break;
    }

    if (world->unexplored_cells.size() == 0)
    {
      finished = true;
      ROS_INFO("Explored the whole environment.");
      break;
    }

    // mark all the cells visited by other drones as explored
    for (auto &it: received)
    {
      Cell* c = world->cells[it.y][it.x];
      tree_starting_points[name_to_ID(it.uav_name)] = c; // the last received position is the new start
      c->cell_status = Cell_status::EXPLORED;
      //c->spanning_trees.insert(name_to_ID(it.uav_name));
      world->erase_from_unexplored(c);
    }
    received.clear();

    ROS_INFO_THROTTLE(5,"Remaining unexplored cells: %d/%d.", (int) world->unexplored_cells.size(), world->width * world->height);
    if (world->unexplored_cells.size() < 5)
    {
      std::cout << "Unexplored cells: ";
      for (auto &it: world->unexplored_cells)
      {
        std::cout << "[" << it->x << "," << it->y << "]";
      }
      std::cout << std::endl;
    }
    bump_rate.sleep();
  }

  if (!finished)
  {
    obstacle_ready = 0; 
    int more = 0;
    while (obstacle_ready < drone_num || more++ < 10)
    {
      msg.x = -1;
      msg.y = -1;
      msg.uav_name = uav_name + "_";
      ros::Duration(0.1).sleep();
      broadcast_obstacle(msg);
      ros::spinOnce();
    }

    ros::spinOnce();
    // mark all the cells visited by other drones as explored
    for (auto &it: received)
    {
      Cell* c = world->cells[it.y][it.x];
      tree_starting_points[name_to_ID(it.uav_name)] = c; // the last received position is the new start
      c->cell_status = Cell_status::EXPLORED;
      //c->spanning_trees.insert(name_to_ID(it.uav_name));
      world->erase_from_unexplored(c);
    }
    received.clear();

    // clear the spanning trees
    spanning_tree.cells.clear();
    spanning_tree.cells.push_back(world->cells[current_pos->y][current_pos->x]);
    world->cells[current_pos->y][current_pos->x]->spanning_trees.insert(spanning_tree.get_id());


    for (int i = 0; i < other_spanning_trees.size(); ++i)
    {
      Spanning_tree *S = &other_spanning_trees[i]; 
      S->cells.clear();
      S->cells.push_back(tree_starting_points[S->get_id()]);
      tree_starting_points[S->get_id()]->spanning_trees.insert(S->get_id());
      if (S->cells.size() == 0)
      {
        ROS_ERROR("Empty tree!");
        return EXIT_FAILURE;
      }
    }

    redundant_cells.clear();
    if (prev_ID == highest_idx) my_turn = true;
    else my_turn = false;

    current_pos = world->cells[current_pos->y][current_pos->x];
    ROS_INFO("Prepared the world for replanning.");
    world->visualise(current_pos);
    start_x = current_pos->x;
    start_y = current_pos->y;
    ros::Duration(1).sleep(); // wait for the other drones to get_ready
    received_ids.clear();
  }
  }
  else
  {
    if (client.call(srv)) ROS_INFO("Trajectory tracking started.");
    else
    {
      ROS_ERROR("Failed to call service.");
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
  }
  }
  ROS_INFO("Task finished, ending.");
  return EXIT_SUCCESS;
}


void pos_change_callback(drone_coverage_planner::pos_change const &msg)
{
  ROS_INFO_THROTTLE(10, "Pos_change messages are arriving.");
  //std::cout << "Received message from " << msg.uav_name <<". Position changed to [" << msg.x << ", " << msg.y << "].";
  std::string s = msg.uav_name + "_" + std::to_string(msg.id);
  if (msg.uav_name != uav_name && std::find(received_ids.begin(), received_ids.end(), s) == received_ids.end())
  {
    received.push_back(msg);
    received_ids.insert(s);
  }
}

void wait_for_message(int check_frequency, drone_coverage_planner::pos_change& message)
{
  while (received.size() == 0)
  {
    broadcast(message);
    ros::Duration(1 / (float) check_frequency).sleep();
    ros::spinOnce();
    ROS_INFO_THROTTLE(3, "Waiting for pos change message.");
  }
}

bool set_algo(drone_coverage_planner::SetAlgorithm::Request &req, drone_coverage_planner::SetAlgorithm::Response &res)
{
  // set the global variable which represents the used algorithm
  if (req.algorithm_name == "CG") 
  {
    spanning_tree_algo = false;
  }
  else if (req.algorithm_name == "ST")
  {
    spanning_tree_algo = true;
  }
  else
  {
    ROS_ERROR("Unknown algorithm name. Please choose between 'ST' and 'CG' algorithms.");
    return false;
  }
  ROS_INFO("The algorithm to be used set to %s.", (spanning_tree_algo?"'Spanning Tree Coverage Algorithm'":"'Cycle Growing Algorithm'") );
  return true;
}

bool set_active(drone_coverage_planner::Explore::Request &req, drone_coverage_planner::Explore::Response &res)
{
  if (req.algorithm == "CG") 
  {
    spanning_tree_algo = false;
  }
  else if (req.algorithm == "ST")
  {
    spanning_tree_algo = true;
  }
  else
  {
    ROS_WARN("Unknown algorithm parameter of the 'Explore' service. Using the previously set algorithm.");
  }
  active = true;
  return true;
}

void receive_pos_change(std::map<std::string, int>& name_to_index, std::vector<Cell*>& redundant_cells, std::vector<Spanning_tree>& other_spanning_trees, World* world, bool& end, bool& my_turn, std::string& prev_uav, drone_coverage_planner::pos_change &msg)
{
  wait_for_message(100, msg);
  for (auto it: received)
  {
    //ROS_INFO("Received a message with pos change.");
    int idx = name_to_ID(it.uav_name);
    int idx_ST_vector = name_to_index[it.uav_name];
    Cell* new_cell = world->cells[it.y][it.x];
    new_cell->spanning_trees.insert(name_to_ID(it.uav_name));
    if (new_cell->spanning_trees.size() > 1) redundant_cells.push_back(new_cell);
    other_spanning_trees[idx_ST_vector].cells.push_back(new_cell); 
    new_cell->cell_status = Cell_status::EXPLORED;
    world->erase_from_unexplored(new_cell);
    std::cout << "Received position change [" << it.x << ", " << it.y << "] from uav " << it.uav_name <<"." << std::endl;
    
    if (world->unexplored_cells.size() <= 0)
    {
      end = true;
      my_turn = true;
      //ROS_INFO("There are no unexplored cells left.");
      break;
    }

    if (it.uav_name == prev_uav)
    {
      my_turn = true;
    }
  }
  received.clear();
}

void odom_callback(mrs_msgs::UavState::ConstPtr const &msg)
{
	std::lock_guard<std::mutex> lock(uav_odom_mutex);
  uav_odom = msg;
  ROS_INFO_THROTTLE(10, "Odometry data are ariving.");
}

void bumper_callback(const mrs_msgs::ObstacleSectors_<std::allocator<void>>::ConstPtr & msg)
{
  if (msg->sector_sensors[0] != -1)
  {
    bumper_msg = msg;
    ROS_INFO_THROTTLE(10, "Bumper data are ariving.");
  }
  else ROS_ERROR_THROTTLE(10, "The drone does not have 2D lidar.");
}

float get_heading()
{
  tf2::Quaternion orientation (uav_odom->pose.orientation.x, uav_odom->pose.orientation.y, uav_odom->pose.orientation.z, uav_odom->pose.orientation.w);
  return orientation.getAngle();
}

void obstacle_callback(drone_coverage_planner::pos_change const &msg)
{
  if (msg.x == -1 || msg.y == -1)
  {
    if (std::find(received_ids.begin(), received_ids.end(), msg.uav_name) == received_ids.end())
    {
      received_ids.insert(msg.uav_name);
      ++obstacle_ready;
    }
  }
  else if (global_world != NULL)
  {
    ROS_INFO("Received obstacle info [%d, %d]", msg.x, msg.y);
    Cell* c = global_world->cells[msg.y][msg.x];
    if (c->cell_status != Cell_status::OBSTACLE)
    {
      c->cell_status = Cell_status::OBSTACLE;
      global_world->erase_from_unexplored(c);
      reset = true;
    }
  }
}

bool check_obstacles(float &obs_x, float &obs_y, float env_x1, float env_y1)
{
  for (int i = 0; i < bumper_msg->sectors.size(); ++i)
  {
    if (bumper_msg->sector_sensors[i] != -1 && bumper_msg->sectors[i] > -0.1 && bumper_msg->sectors[i] < cell_size*(OBSTACLE_TRIGGER_DIST + 0.5))
    {
      float angle = get_heading() + (float) i*(float) (2*M_PI / bumper_msg->sectors.size());
      while (angle > 2*M_PI) angle -= 2*M_PI;
      obs_x = (uav_odom->pose.position.x + cos(angle)*bumper_msg->sectors[i] - env_x1)/cell_size;
      obs_y = (uav_odom->pose.position.y + sin(angle)*bumper_msg->sectors[i] - env_y1)/cell_size;
      return true;
      
    }
  }
  return false;
}

void increment_highest_id(drone_coverage_planner::pos_change &message)
{
  for (auto &pub: pos_change_publishers)
  {
    for (int i = 0; i < 5; ++i)
    {
      pub.publish(message);
    }
  }
  ++highest_id;
}

void create_publishers_pos_change(std::vector<std::string>& names, ros::NodeHandle& n)
{
  for (auto &name: names)
  {
    // initialize the spanning trees of other uavs
    std::string pos_change_topic = name + "/pos_change";
    ros::Publisher  pos_change_pub   = n.advertise<drone_coverage_planner::pos_change>(pos_change_topic, 100, true);
    pos_change_publishers.push_back(pos_change_pub);
    ros::Subscriber pos_change_sub  = n.subscribe(pos_change_topic, 100, pos_change_callback);
    subscribers.push_back(pos_change_sub);
  }
}

void create_publishers_obstacles(std::vector<std::string>& names, ros::NodeHandle& n)
{
  for (auto &name: names)
  {
    // initialize the spanning trees of other uavs
    std::string obstacle_topic = name + "/obstacles";
    ros::Publisher  obstacle_pub   = n.advertise<drone_coverage_planner::pos_change>(obstacle_topic, 10, true);
    obstacle_publishers.push_back(obstacle_pub);
    ros::Subscriber obstacle_sub  = n.subscribe(obstacle_topic, 10, obstacle_callback);
    subscribers_obstacles.push_back(obstacle_sub);
  }
}

void broadcast(drone_coverage_planner::pos_change& message)
{
  for (auto &pub: pos_change_publishers)
  {
    pub.publish(message);
  }
}

void broadcast_obstacle(drone_coverage_planner::pos_change& message)
{
  for (auto &pub: obstacle_publishers)
  {
    pub.publish(message);
  }
}
