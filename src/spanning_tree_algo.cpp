#include <vector>
#include <string>
#include <unordered_map>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <mrs_msgs/TrajectoryReference.h>

#include "World.h"
#include "Cell.h"
#include "cell_functions.h"

enum edge_dir {up, right, down, left};
enum it_state {u, r, d, l};

int priority_r[4] = {edge_dir::up, edge_dir::right, edge_dir::down, edge_dir::left};
int priority_d[4] = {edge_dir::right, edge_dir::down, edge_dir::left, edge_dir::up};
int priority_l[4] = {edge_dir::down, edge_dir::left, edge_dir::up, edge_dir::right};
int priority_u[4] = {edge_dir::left, edge_dir::up, edge_dir::right,edge_dir::down};

void visualise_cycle(World* world, std::unordered_map<std::string, std::unordered_set<int>>& cycle);

int uniform_fill(int i, int j, std::vector<std::vector<int>>& world)
{
  int content = world[i][j];
  if (world[i+1][j] != content || world[i+1][j+1] != content || world[i][j+1] != content)
  {
    return -1;
  }
  else
  {
    return content;
  }
}

bool  approximate_world(std::vector<std::vector<int>>& old_world, std::vector<std::vector<int>>& new_world)
// check whether it is possible to approximate given world and if so, do so
{
  if (old_world.size() % 2 != 0) return false;
  if (old_world[0].size() % 2 != 0) return false;
  //std::cout << "World dimensions checked." << std::endl;
  for (int i = 0; i < old_world.size() - 1; i = i + 2)
  {
    std::vector<int> row;
    for (int j = 0; j < old_world[0].size() - 1; j = j + 2)
    {
      int new_cell = uniform_fill(i, j, old_world);
      //std::cout << i << ", " << j << " can be merged into " << new_cell << std::endl;  
      if (new_cell == -1)
      {
        return false;
      }
      row.push_back(new_cell);
    }
    new_world.push_back(row);
  }
  return true;
}

std::string float_to_str(float x, int precision)
{
  std::stringstream stream;
  stream << std::fixed << std::setprecision(precision) << x;
  return stream.str();
}

std::string pos_to_str(float x, float y)
{
  return float_to_str(x, 1) + " " + float_to_str(y, 1);
}

void add_to_cycle(float  x, float y, std::unordered_map<std::string, std::unordered_set<int>>& cycle, int edge, bool replace)
{
  std::string pos = pos_to_str(x, y);
  if (cycle.count(pos) == 0)
  {
    std::unordered_set<int> edges;
    cycle[pos] = edges;
  }
  else if (replace)
  {
    cycle[pos].clear();
  }
  cycle[pos].insert(edge);
}

void build_horizontal_tree(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle)
{
  std::unordered_map<std::string, int> colors;
  int highest_color = 0;
  for (int i = 0; i < world->height; ++i)
  {
    Cell* last = NULL;
    for (int j = 0; j < world->width; ++j)
    {
      Cell* current_cell = world->cells[i][j];
      if (current_cell->in_spanning_tree(st_ID))
      {
        //ROS_INFO("Current cell is [%d, %d].",current_cell->x, current_cell->y);
        //if (last != NULL) ROS_INFO("Difference is %d.", current_cell->x - last->x);
        if ((last != NULL) && (current_cell->x - last->x == 1))
        {
          //ROS_INFO("Making edge between [%d, %d] and [%d, %d].", current_cell->x, current_cell->y, last->x, last->y);
          make_edge(current_cell, last);
          add_to_cycle((float) (last->x + 0.5), (float) last->y,  cycle, edge_dir::right, false);
          add_to_cycle((float) j, (float) i,  cycle, edge_dir::right, false);
          add_to_cycle((float) (j + 0.5), (float) (i + 0.5),  cycle, edge_dir::left, false);
          add_to_cycle((float) j, (float) (i + 0.5),  cycle, edge_dir::left, false);
          //std::cout << i <<  ", " << j << "is normal cell." << std::endl;
        }
        else
        {
          ++highest_color;
          add_to_cycle((float)j, (float) i,  cycle, edge_dir::right, false);
          add_to_cycle((float)(j + 0.5), (float) (i + 0.5),  cycle, edge_dir::left, false);
          add_to_cycle((float) j, (float) (i + 0.5),  cycle, edge_dir::up, false);
          std::cout << i <<  ", " << j << " is connection cell." << std::endl;
        }
        last = current_cell;
        colors[pos_to_str((float)j, (float) i)] = highest_color;
        //ROS_INFO("Last cell is now [%d, %d].", last->x, last->y);
      }
      else if ((last != NULL) && current_cell->x - last->x == 1)
      {
        add_to_cycle((float) (last->x + 0.5), (float)last->y, cycle, edge_dir::down, false);
        //std::cout << "Added down edge to " <<  last->y << ", " << last->x + 0.5 << std::endl;
      }
    }
    
    if (world->cells[i][world->width - 1]->in_spanning_tree(st_ID))
    {
      add_to_cycle((float) (last->x + 0.5), (float) last->y, cycle, edge_dir::down, false);
      //  std::cout << "Added down edge to " <<  last->y << ", " << last->x + 0.5 << std::endl;
    }
  }

  std::cout << "Starting connection making process." << std::endl;
  for (int j = 0; j < world->width; ++j)
  {
    for (int i = 0; i < world->height-1; ++i)
    {
      std::string pos_high = pos_to_str((float) j, (float) i);
      std::string pos_low = pos_to_str((float) j, (float) i+1);

      if (colors.find(pos_high) == colors.end() || colors.find(pos_low) == colors.end()) continue;
      int color1 = colors[pos_high];
      int color2 = colors[pos_low];
      if (color1 == color2) continue;
      make_edge(world->cells[i][j], world->cells[i+1][j]);
      add_to_cycle((float) (j + 0.5),(float) (i + 0.5), cycle, edge_dir::down, true);
      add_to_cycle((float) j, (float) i + 1, cycle, edge_dir::up, true);
      int new_c = std::min(color1, color2);
      int old_c = std::max(color1, color2);
      for (auto& it: colors)
      {
        if (it.second == old_c) colors[it.first] = new_c;
      }
    }
  }
  visualise_cycle(world, cycle);
}


void build_vertical_tree(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle)
{
  std::unordered_map<std::string, int> colors;
  int highest_color = 0;
  for (int j = 0; j < world->width; ++j)
  {
    Cell* last = NULL;
    for (int i = 0; i < world->height; ++i)
    {
      Cell* current_cell = world->cells[i][j];
      if (current_cell->in_spanning_tree(st_ID))
      {
        //ROS_INFO("Current cell is [%d, %d].",current_cell->x, current_cell->y);
        //if (last != NULL) ROS_INFO("Difference is %d.", current_cell->x - last->x);
        if ((last != NULL) && (current_cell->y - last->y == 1))
        {
          //ROS_INFO("Making edge between [%d, %d] and [%d, %d].", current_cell->x, current_cell->y, last->x, last->y);
          make_edge(current_cell, last);
          add_to_cycle((float)last->x, (float) (last->y + 0.5),  cycle, edge_dir::down, false);
          add_to_cycle((float) j, (float) i,  cycle, edge_dir::down, false);
          add_to_cycle((float) (j + 0.5), (float) (i + 0.5),  cycle, edge_dir::up, false);
          add_to_cycle((float) (j+0.5),(float) i,  cycle, edge_dir::up, false);
        }
        else
        {
          ++highest_color;
          add_to_cycle((float) j, (float) i,  cycle, edge_dir::down, false);
          add_to_cycle((float) (j + 0.5), (float) i,  cycle, edge_dir::left, false);
          add_to_cycle((float) (j + 0.5), (float) (i + 0.5),  cycle, edge_dir::up, false);
        }
        last = current_cell;
        colors[pos_to_str((float)j, (float) i)] = highest_color;
        //ROS_INFO("Last cell is now [%d, %d].", last->x, last->y);
      }
      else if ((last != NULL) && current_cell->y - last->y == 1)
      {
        add_to_cycle((float) last->x, (float) (last->y + 0.5), cycle, edge_dir::right, false);
      }
    }

    if (world->cells[world->height-1][j]->in_spanning_tree(st_ID))
    {
      add_to_cycle((float) last->x, (float) (last->y + 0.5), cycle, edge_dir::right, false);
    }
  }

  std::cout << "Starting connection making process." << std::endl;
  for (int i = 0; i < world->height; ++i)
  {
    for (int j = 0; j < world->width-1; ++j)
    {
      std::string pos_left = pos_to_str((float) j, (float) i);
      std::string pos_right = pos_to_str((float) j+1, (float) i);

      if (colors.find(pos_left) == colors.end() || colors.find(pos_right) == colors.end()) continue;
      int color1 = colors[pos_left];
      int color2 = colors[pos_right];
      if (color1 == color2) continue;

      make_edge(world->cells[i][j], world->cells[i][j+1]);
      add_to_cycle((float) (j + 0.5), (float) (i + 0.5), cycle, edge_dir::right, true);
      add_to_cycle((float) j + 1, (float) i, cycle, edge_dir::left, true);

      int new_c = std::min(color1, color2);
      int old_c = std::max(color1, color2);

      
      for (auto& it: colors)
      {
        if (it.second == old_c) colors[it.first] = new_c;
      }
    }
  }
  visualise_cycle(world, cycle);
}

void add_point(std::vector<mrs_msgs::Reference>& trajectory, float heading, float  x, float y, float z, float  env_x1, float env_y1, float cell_size)
{
  mrs_msgs::Reference point;
  point.heading = heading;
  point.position.x = cell_size*x + env_x1;
  point.position.y = cell_size*y + env_y1;
  point.position.z = z;
  trajectory.push_back(point);
}

int find_best_edge(int state, std::unordered_set<int>& edges)
{
  if (edges.size() == 1)
  {
    return *edges.begin();
  }
  else if (edges.size() == 0)
  {
    std::cout << "No possible edge." << std::endl;
    return -1;
  }
  int* priority;
  switch (state)
  {
    case it_state::r:
      priority = priority_r;
      break;
    case it_state::d:
      priority = priority_d;
      break;
    case it_state::l:
      priority = priority_l;
      break;
    case it_state::u:
      priority = priority_u;
      break;
    default:
      std::cout << "Invalid state: " << state << std::endl;
  }

  for (int i = 0; i < 4; ++i)
  {
    if (std::find(edges.begin(), edges.end(), priority[i]) != edges.end())
    {
      return priority[i];
    }
  }
  std::cout << "Warning, this should not happen!" << std::endl;
  return -1;
}

 float get_heading(int state)
{
  switch(state)
  {
    case it_state::r:
      return 0;
      break;
    case it_state::d:
      return 3.14/2;
      break;
    case it_state::l:
      return 3.14;
      break;
    case it_state::u:
      return -3.14/2;
      break;
    default:
      return 0;
        break;
  }
}

int update_state(float& cur_x, float& cur_y, int edge, float edge_size)
{
  int ret;
  switch(edge)
  {
    case edge_dir::right:
        cur_x += edge_size;
        ret = it_state::r;
      break;
    case edge_dir::down:
      cur_y += edge_size;
      ret = it_state::d;
      break;
    case edge_dir::left:
        cur_x -= edge_size;
        ret = it_state::l;
      break;
    case edge_dir::up:
      cur_y -= edge_size;
      ret = it_state::u;
      break;
  }
  return ret;
}

void cycle_to_trajectory(mrs_msgs::TrajectoryReference& cmd, std::unordered_map<std::string, std::unordered_set<int>>& cycle, Cell* start, float edge_size, float cell_size, float env_x1, float env_y1, float  z, float speed)
{
   int state = it_state::r; 

  cmd.header.stamp = ros::Time::now();
  cmd.header.seq = 0;
  cmd.header.frame_id = "";//"uav" + std::to_string(uav_id) + "/fcu";  
  cmd.use_heading = true;
  cmd.fly_now = false;
  cmd.loop = false;
  cmd.dt = ((float) cell_size * edge_size)/speed;

  float cur_x = (float) start->x;
  float cur_y = (float) start->y;
  while (true)
  {
    //std::cout << "x " << cur_x << ", y " << cur_y << ", state " << state << std::endl; 
    float heading = get_heading(state);
    add_point(cmd.points, heading, cur_x,cur_y, z, env_x1, env_y1, cell_size);
    std::string cur_name = pos_to_str(cur_x, cur_y);
    int cur_edge = find_best_edge(state, cycle[cur_name]);
    if (cur_edge == -1)
    {
      std::cout << "Invalid edge" << std::endl;
      break;
    }
    //std::cout << "Chosen edge " << cur_edge << std::endl;
    state = update_state(cur_x, cur_y, cur_edge, edge_size); 
    if (cur_x == (float) start->x && cur_y == (float) start->y)
    {
      break;
    }
  }
  add_point(cmd.points, 0, cur_x,cur_y, z, env_x1, env_y1, cell_size);
}

std::vector<Cell*> tree_to_traj(Cell* current_pos, int start_x, int start_y, World* world)
{
  // convert the generated spanning tree into a trajectory
  current_pos = world->cells[start_y][start_x]; 
  current_pos->steps_from_start = 0;
  current_pos->visited = true;
  std::vector<Cell*> cell_trajectory;
  std::vector<std::tuple<int, int>> potential_redundancy;
  do 
  {
    cell_trajectory.push_back(current_pos);
    //current_pos->print_edges();
    current_pos = current_pos->go_to_next_cell(start_x, start_y);
      current_pos->count_steps_from_start();
    if (current_pos->x == start_x && current_pos->y == start_y)
    {
      //std::cout << "End reached, testing that all edges were visited." << std::endl;
      Cell* explorer;
      for (int i = (int) cell_trajectory.size() - 1; i >= 0; --i)
      {
        explorer = cell_trajectory[i];
        if (explorer->x == start_x && explorer->y == start_y)
        {
          //std::cout << "All edges inside this trajectory were visited." << std::endl;
          break;
        }
        if (!explorer->all_edges_visited())
        {
          //std::cout << "Found a non visited edge inside the trajectory." << std::endl;
          for (int j = (int) cell_trajectory.size()-1; j >= i; --j)
          {
            cell_trajectory.push_back(cell_trajectory[j]);
          }
          current_pos = explorer->go_to_next_cell(start_x, start_y);
          current_pos->count_steps_from_start();
          potential_redundancy.push_back(std::make_tuple(i, cell_trajectory.size()-1));
          break;    
        }
      }
    }
      //ROS_INFO("Visited [%d, %d].", current_pos->x, current_pos->y);
  }
  while((current_pos->x != start_x ||  current_pos->y != start_y) || current_pos->all_edges_visited() == false );
  return cell_trajectory;
}

// cycle growing algo

std::unordered_set<int> get_possible_edges(int x, int y, World* world, int st_ID)
{
  std::unordered_set<int> edges;
  if (x < world->cells[y].size() - 1 && world->cells[y][x+1]->in_spanning_tree(st_ID))
  {
    edges.insert(edge_dir::right);
  }
  if (x > 0 && world->cells[y][x-1]->in_spanning_tree(st_ID))
  {
    edges.insert(edge_dir::left);
  }
  if (y > 0 && world->cells[y-1][x]->in_spanning_tree(st_ID))
  {
    edges.insert(edge_dir::up);
  }
  if (y < world->cells.size() - 1 && world->cells[y+1][x]->in_spanning_tree(st_ID))
  {
    edges.insert(edge_dir::down);
  }
  return edges;
}

int get_next_edge(int x, int y, World* world, int state, int st_ID)
{
  std::unordered_set<int> edges = get_possible_edges(x, y, world, st_ID);
  return find_best_edge(state, edges);
}

int neighbor_in_cycle(int i, int j, bool horizontal, std::unordered_map<std::string, std::unordered_set<int>>& cycle, World* world, int st_ID)
{
  int m = world->cells.size();
  int n = world->cells[i].size();
  if (horizontal)
  {
    if (!world->cells[i][j]->in_spanning_tree(st_ID) || !world->cells[i][j+1]->in_spanning_tree(st_ID) || cycle.count(pos_to_str((float)j,(float) i)) > 0 || cycle.count(pos_to_str((float) j+1,(float) i)) > 0) 
    {
      return -1; // it is not possible to append this pair to cycle
    }
    if (i > 0 && cycle.count(pos_to_str((float) j, (float) i - 1)) > 0 && cycle.count(pos_to_str((float) j+1, (float) i - 1)) > 0)
		{
			return edge_dir::up; // it is possible to append it to upper segment	
		}
    if (i < m-1 && cycle.count(pos_to_str((float) j, (float) i + 1)) > 0 && cycle.count(pos_to_str((float) j+1, (float) i + 1)) > 0)
		{
			return edge_dir::down; // it is possible to append it to lower segment
		}
  }
  else
  {
    if (!world->cells[i][j]->in_spanning_tree(st_ID) || !world->cells[i+1][j]->in_spanning_tree(st_ID) || cycle.count(pos_to_str((float)j,(float) i)) > 0 ||  cycle.count(pos_to_str((float) j,(float) i+1)) > 0) 
    {
      return -1; // it is not possible to append this pair to cycle
    }
    if (j > 0 && cycle.count(pos_to_str((float) j-1, (float) i)) > 0 && cycle.count(pos_to_str((float) j-1, (float) i + 1)) > 0)
		{
			return edge_dir::left; // it is possible to append it to left segment	
		}
    if (j < n-1 && cycle.count(pos_to_str((float) j + 1, (float) i)) > 0 && cycle.count(pos_to_str((float) j+1, (float) i + 1)) > 0)
		{
			return edge_dir::right; // it is possible to append it to right segment
		}
  }
  std::cout << "End of 'neighbor_in_cycle' reached. i = " << i << " j = " << j << (horizontal?" horizontal":" vertical") << std::endl;
	return -1;
}

int get_dual_edge(int edge)
{
  switch(edge)
  {
    case edge_dir::down:
      return edge_dir::up;
      break;
    case edge_dir::up:
      return edge_dir::down;
      break;
    case edge_dir::left:
      return edge_dir::right;
      break;
    case edge_dir::right:
      return edge_dir::left;
      break;
    default:
      std::cout << "Warning, this should not ever happen." << std::endl;
      return edge_dir::right;
  }
}

void horizontal_grow(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle)
{
  for (int i = 0; i < world->cells.size(); ++i)
  {
    for (int j = 0; j < world->cells[i].size() - 1; ++j)
    {
      int edge = neighbor_in_cycle(i, j, true, cycle, world, st_ID);
      if (edge < 0)
      {
        continue;
      }
      std::cout << "Found a pair that can be appended to the cycle. x = " << j << " , y = " << i << std::endl;
      int k = 0;
      bool mode_2 = false;
      float cur_x = (float) j;
      float cur_y = (float) i;
      update_state(cur_x, cur_y, edge, 1);
      std::unordered_set<int>& edges = cycle[pos_to_str(cur_x , cur_y)];
      std::unordered_set<int>& neigh_edges = cycle[pos_to_str(cur_x + 1, cur_y)];
      
      auto it = std::find(edges.begin(), edges.end(), edge_dir::right);
      auto it2 = std::find(neigh_edges.begin(), neigh_edges.end(), edge_dir::left);
      if (it != edges.end())
      {
        std::cout << "Mode 1" << std::endl;
        edges.erase(it);
      }
      else if (it2 != neigh_edges.end())
      {
        std::cout << "Mode 2" << std::endl;
        neigh_edges.erase(it2);
        mode_2 = true;
      }
      else
      {
        std::cout << "Could not find the proper edges." << std::endl;
        continue;
      }

      int first_edge = edge;
      while (edge >= 0)
      {
        if (!mode_2)
        {
          add_to_cycle((float) j, (float) (i + k - 1), cycle, get_dual_edge(edge), false);
          add_to_cycle((float) j+1, (float) (i + k), cycle, edge, true);
          cycle[pos_to_str((float) j, (float)(i + k))] = std::unordered_set<int> {};
        }
        else
        {
          add_to_cycle((float) j+1, (float) (i + k + 1), cycle, get_dual_edge(edge), false);
          add_to_cycle((float) j, (float) (i + k), cycle, edge, true);
          cycle[pos_to_str((float) j + 1, (float)(i + k))] = std::unordered_set<int> {};
        }
        if (edge == edge_dir::up)
        {
          ++k;
        }
        else
        {
          --k;
        }
        edge = neighbor_in_cycle(i + k, j, true, cycle, world, st_ID);
        //std::cout << "New edge is " << edge << std::endl;
      }
      std::cout << "Appended " << k << " pairs." << std::endl;
      if (first_edge == edge_dir::up)
      {
        --k;
      }
      else
      {
        ++k;
      }
      if (!mode_2)
      {
        add_to_cycle((float) j, (float) (i+k), cycle, edge_dir::right, false);
      }
      else
      {
        add_to_cycle((float) j + 1, (float) (i+k), cycle, edge_dir::left, false);
      }
      
    }
  }
}

void vertical_grow(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle)
{
  for (int i = 0; i < world->cells.size()-1; ++i)
  {
    for (int j = 0; j < world->cells[i].size(); ++j)
    {
      int edge = neighbor_in_cycle(i, j, false, cycle, world, st_ID);
      if (edge < 0)
      {
        continue;
      }
      std::cout << "Found a pair that can be appended to the cycle. x = " << j << " , y = " << i << std::endl;
      int k = 0;
      float cur_x = (float) j;
      float cur_y = (float) i;
      update_state(cur_x, cur_y, edge, 1);
      bool mode_2 = false;
      std::unordered_set<int>& edges = cycle[pos_to_str(cur_x , cur_y)];
      std::unordered_set<int>& neigh_edges = cycle[pos_to_str(cur_x, (float) cur_y + 1)];
      
      auto it = std::find(edges.begin(), edges.end(), edge_dir::down);
      auto it2 = std::find(neigh_edges.begin(), neigh_edges.end(), edge_dir::up);
      if (it != edges.end())
      {
        std::cout << "Mode 1" << std::endl;
        edges.erase(it);
      }
      else if (it2 != neigh_edges.end())
      {
        std::cout << "Mode 2" << std::endl;
        std::cout << "Neighbour edges before: ";
        for (auto &e: neigh_edges)
        {
          std::cout << e << " ";
        }
        std::cout << std::endl;
        neigh_edges.erase(it2);
        std::cout << "Neighbour edges after: ";
        for (auto &e: neigh_edges)
        {
          std::cout << e << " ";
        }
        std::cout << std::endl;
        mode_2 = true;
      }
      else
      {
        std::cout << "Could not find the proper edges." << std::endl;
        continue;
      }

      int first_edge = edge;
      while (edge >= 0)
      {
        if (!mode_2)
        {
          add_to_cycle((float) (j + k + 1), (float) i, cycle, get_dual_edge(edge), false);
          add_to_cycle((float) (j+k), (float) i + 1, cycle, edge, true);
          cycle[pos_to_str((float) (j + k), (float) i)] = std::unordered_set<int> {};
        }
        else
        {
          add_to_cycle((float) (j + k - 1), (float) i + 1, cycle, get_dual_edge(edge), false);
          add_to_cycle((float) (j + k), (float) i, cycle, edge, true);
          cycle[pos_to_str((float) (j + k), (float) i + 1)] = std::unordered_set<int> {};
        }
        if (edge == edge_dir::left)
        {
          ++k;
        }
        else
        {
          --k;
        }
        edge = neighbor_in_cycle(i, j + k, false, cycle, world, st_ID);
        std::cout << "Cell [" << j+k << ", " << i << "], edge = "<< edge << std::endl;
      }
      std::cout << "Appended " << k << " pairs." << std::endl;
      if (first_edge == edge_dir::left)
      {
        --k;
      }
      else
      {
        ++k;
      }
      if (!mode_2)
      {
        add_to_cycle((float) (j + k), (float) i, cycle, edge_dir::down, false);
      }
      else
      {
        add_to_cycle((float) (j + k), (float) i + 1, cycle, edge_dir::up, false);
      }
    
    }
  }
}

void str_to_pos(float& x, float& y, std::string name)
{
  std::stringstream ss(name);
  ss >> x; 
  ss >> y;
  //std::cout << name << " converted to x = " << x << " y = " << y << std::endl;
}

void visualise_cycle(World* world, std::unordered_map<std::string, std::unordered_set<int>>& cycle)
{
  std::cout << "The cycle is:" << std::endl;
  for (int i = 0; i < world->height; ++i)
  {
    for (int j = 0; j < world->width; ++j)
    {
      if (cycle.count(pos_to_str(j, world->height-1-i)) > 0)
      {
        std::cout << "X ";
      }
      else std::cout << "0 ";
    }
    std::cout << std::endl;
  }
}

void explore_CG(World* world, int st_ID, std::unordered_map<std::string, std::unordered_set<int>>& cycle, std::unordered_map<std::string, std::unordered_set<int>>& priority_edges, std::vector<std::tuple<int, int, int>>& additional_edges, bool horizontal)
{
  // step 1: Find the biggest cycle
  Cell* start= NULL;
  for (int i = 0; i < world->cells.size(); ++i)
  {
    for (int j = 0; j < world->cells[i].size(); ++j)
    {
      Cell* current = world->cells[i][j];
      if (current->in_spanning_tree(st_ID))
      {
        start = current;
        break;
      }
    }
    if (start)
    {
      break;
    }
  }
  if (start == NULL)
    //basecase - all cells explored
  {
    return;
  }

  std::cout << "Started new run of the algorithm." << std::endl;
  world->visualise(start);
  std::cout << "Start found " << start->x << ", " << start->y << std::endl;

  // filter out bad starting positions
  int i = 0;
  std::vector<Cell*> frontier;
  std::unordered_set<Cell*> known;
  frontier.push_back(start);
  bool bad_start = false;
  Cell* better_start = NULL;
  while (i < 20) 
  {
    Cell* cur = frontier[i++];
    known.insert(cur);
    std::unordered_set<int> edges = get_possible_edges(cur->x, cur->y, world, st_ID);
    if (edges.size() > 2)
    {
      if (better_start == NULL && i > 1) better_start = cur;
    }
    else if (edges.size() <= 1)
    {
      bad_start = true;
    }
    else if (edges.size() == 2)
    {
      for (auto &e: edges)
      {
        float neigh_x = (float) cur->x;
        float neigh_y = (float) cur->y;
        update_state(neigh_x, neigh_y, e, 1);
        if(std::find(known.begin(), known.end(), world->cells[neigh_y][neigh_x]) == known.end()) frontier.push_back(world->cells[neigh_y][neigh_x]);
      }
    }
    if (i == frontier.size()) break;
  }
  if (bad_start && better_start != NULL) start = better_start;

  std::cout << "New start is [ " << start->x << ", " << start->y << "]." << std::endl;
  float cur_x = (float) start->x;
  float cur_y = (float) start->y;
  int state = it_state::r;
  cycle[pos_to_str(cur_x, cur_y)] = std::unordered_set<int>{}; 
  int k = 0;
  while (true && k < 100000)
  {
    ++k;
    int next_edge = get_next_edge((int) cur_x, (int) cur_y, world, state, st_ID);
    if (next_edge == -1)
    {
      std::cout << "Invalid edge, x = " << cur_x << " , y = " << cur_y << std::endl;
      break;
    }
    add_to_cycle(cur_x, cur_y, cycle, next_edge, false);
    state = update_state(cur_x, cur_y, next_edge, 1); 
    //std::cout << "Updated to x = " << cur_x << " y = " << cur_y << " with edge " << next_edge << std::endl;
    if (cur_x == (float)start->x && cur_y == (float) start->y)
    {
      if (better_start != NULL) better_start = NULL; // the start should be crossed more times
      else break;
    }
  }
  visualise_cycle(world, cycle);

  // step 2: grow the cycle
  if (horizontal)
  {
    std::cout << "Starting horizontal growing." << std::endl;
    // horizontal parts
    horizontal_grow(world, st_ID, cycle);
    // vertical parts
    vertical_grow(world, st_ID, cycle);
  }
  else
  {
    std::cout << "Starting vertical growing." << std::endl;
    // vertical parts
    vertical_grow(world, st_ID, cycle);
    // horizontal parts
    horizontal_grow(world, st_ID, cycle);
  }
   
  //step 3: connect the neighbor cells that can be connected
  std::vector<std::tuple<int, int, int>> new_additional_edges;
  
  for (int i = 0; i < world->cells.size(); ++i)
  {
    for (int j = 0; j < world->cells[i].size(); ++j)
    {
      if (world->cells[i][j]->in_spanning_tree(st_ID) && cycle.count(pos_to_str((float) j, (float) i)) == 0)
      {
        std::unordered_set<int> edges = get_possible_edges(j, i, world, st_ID);
        for (auto e: edges)
        {
          float neigh_x = (float) j;
          float neigh_y = (float) i;
          update_state(neigh_x, neigh_y, e, 1);
          if (cycle.count(pos_to_str(neigh_x, neigh_y)) > 0)
          {
            add_to_cycle(neigh_x, neigh_y, cycle, get_dual_edge(e), false);
            add_to_cycle(neigh_x, neigh_y,priority_edges, get_dual_edge(e), false);
            new_additional_edges.push_back(std::tuple(i, j, e));
            break;
          }
        }
      }
    }
  }
  std::cout << "After growing and neighbor cell connection:" << std::endl;
  visualise_cycle(world, cycle);

  // now add the additional edges to cycle
  for (auto x: new_additional_edges)
  {
    add_to_cycle((float) std::get<1>(x), (float) std::get<0>(x), cycle, std::get<2>(x), true);
    additional_edges.push_back(x);
  }

  // mark all cells in cycle as explored
  for (const auto &pair: cycle)
  {
    float x;
    float y;
    str_to_pos(x, y, pair.first);
    world->cells[y][x]->spanning_trees.erase(st_ID);
    //std::cout << "x = " << x << " y = " << y << " in spanning tree " << st_ID << (world->cells[y][x]->in_spanning_tree(st_ID)?" True":" False") << std::endl;
  }
  //step 4: repeat on unexplored parts and connect them
  std::unordered_map<std::string, std::unordered_set<int>> subcycle;
  std::unordered_map<std::string, std::unordered_set<int>> new_priority_edges;

  explore_CG(world, st_ID, subcycle, new_priority_edges, additional_edges, horizontal);

  // restore the cells into spanning tree 
  for (const auto &pair: cycle)
  {
    float x;
    float y;
    str_to_pos(x, y, pair.first);
    world->cells[y][x]->spanning_trees.insert(st_ID);
    //std::cout << "x = " << x << " y = " << y << " in spanning tree " << st_ID << (world->cells[y][x]->in_spanning_tree(st_ID)?" True":" False") << std::endl;
  }

  bool connected = false;
  for (auto x: additional_edges)
  {
    std::vector<int> edges = {edge_dir::down, edge_dir::right, edge_dir::left, edge_dir::up};
    for (int e: edges)
    {
      float cur_x = (float) std::get<1>(x);
      float cur_y = (float) std::get<0>(x);
      update_state(cur_x, cur_y, e, 1);
      if (subcycle.count(pos_to_str(cur_x, cur_y))> 0)
      {
        add_to_cycle((float) std::get<1>(x), (float) std::get<0>(x), cycle, e, false);
        add_to_cycle((float) std::get<1>(x), (float) std::get<0>(x), priority_edges, e, false);
        add_to_cycle(cur_x, cur_y, subcycle, get_dual_edge(e), false);
        connected = true;
        std::cout << "Connected new subcycle with x = " << (float) std::get<1>(x) << " y = " << (float) std::get<0>(x) << " and edge " << e << std::endl;
        break;
      }
    }
    if (connected) break;
  }

  // now merge cycle and subcycle
  cycle.insert(subcycle.begin(), subcycle.end());

  // now merge priority_edges and new_priority_edges
  priority_edges.insert(new_priority_edges.begin(), new_priority_edges.end());
  std::cout << "Cycle after everything:" << std::endl;
  visualise_cycle(world, cycle);
}
  
int find_best_edge_GC(int state, std::unordered_set<int>& edges, std::unordered_map<std::string, std::unordered_set<int>>& priority_edges, std::string cur_name)
{

  if (priority_edges.count(cur_name)>0 && priority_edges[cur_name].size() > 0)
  {
    int ret = *priority_edges[cur_name].begin();
    priority_edges[cur_name].erase(priority_edges[cur_name].begin());
    return ret;
  }
  else
  {
    return find_best_edge(state, edges);
  }
  std::cout << "End of 'find_best_edge_GC' reached, should not happen." << std::endl;
  return -1;
}

void cycle_to_trajectory_GC(mrs_msgs::TrajectoryReference& cmd, std::unordered_map<std::string, std::unordered_set<int>>& cycle, std::unordered_map<std::string, std::unordered_set<int>>& priority_edges, Cell* start, float edge_size, float cell_size, float env_x1, float env_y1, float z, float speed)
{
  int state = it_state::r; 

  cmd.header.stamp = ros::Time::now();
  cmd.header.seq = 0;
  cmd.header.frame_id = "";//"uav" + std::to_string(uav_id) + "/fcu";  
  cmd.use_heading = true;
  cmd.fly_now = false;
  cmd.loop = false;
  cmd.dt = edge_size * (float) cell_size/speed;

  float cur_x = (float) start->x;
  float cur_y = (float) start->y;
  int cur_start_visits = 1;
  const int start_visits = cycle[pos_to_str(cur_x, cur_y)].size();
  while (true)
  {
    //std::cout << "x " << cur_x << ", y " << cur_y << ", state " << state << std::endl; 
    float heading = get_heading(state);
    add_point(cmd.points, heading, cur_x,cur_y, z, env_x1, env_y1, cell_size);
    std::string cur_name = pos_to_str(cur_x, cur_y);
    int cur_edge = find_best_edge_GC(state, cycle[cur_name], priority_edges, cur_name);
    if (cur_edge == -1)
    {
      std::cout << "Invalid edge" << std::endl;
      break;
    }
    //std::cout << "Chosen edge " << cur_edge << std::endl;
    state = update_state(cur_x, cur_y, cur_edge, edge_size); 
    if (cur_x == (float) start->x && cur_y == (float) start->y)
    {
      ++cur_start_visits;
      std::cout << "Start visited for " << cur_start_visits << " time." << std::endl;
      if (cur_start_visits > start_visits) break;
    }
  }
  add_point(cmd.points, 0, cur_x,cur_y, z, env_x1, env_y1, cell_size);
}
