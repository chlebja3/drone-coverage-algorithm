#include <Cell.h>
#include <World.h>
#include <math.h>
#include <Spanning_tree.h>
#include <algorithm>
#include <unordered_set>
#include <limits>

float euc_dist_to_center(const float center_x, const float center_y, Cell* cell)
// calculate the Euclidean distance to center of inertia (its 2nd power)
{
  return (float) pow(center_x - (float) cell->x, 2) + pow(center_y - (float) cell->y, 2); 
}


float euc_dist(Cell* cell1, Cell* cell2)
// calculate the Euclidean distance to center of inertia (its 2nd power)
{
  return (float) pow((float)cell1->x - (float) cell2->x, 2) + pow((float) cell1->y - (float) cell2->y, 2); 
}

float man_dist_to_center(const float center_x, const float center_y, Cell* cell)
// calculate the Euclidean distance to center of inertia
{
  return abs(center_x - (float) cell->x) + abs(center_y - (float) cell->y); 
}

float man_dist(Cell* cell1, Cell* cell2)
// calculate the Euclidean distance of two cells 
{
  return abs((float) cell1->x - (float) cell2->x) + abs((float) cell1->y - (float) cell2->y); 
}

float dist_from_tree(Spanning_tree& tree, Cell* cell)
{
  float min_dist = man_dist(tree.cells[0], cell);
  for (int i = 0; i < tree.cells.size(); ++i)
  {
    float dist = man_dist(tree.cells[i], cell);
    if (dist < min_dist)
    {
      min_dist = dist;
    }
  }
  return min_dist;
}

void add_neighbours(std::vector<Cell*>& frontier, World* world, Cell* cell, std::unordered_set<Cell*> known)
{
  if ( cell-> y > 0)
  {
    Cell* lower = world->cells[cell->y - 1][cell->x];
    if (lower->cell_status != Cell_status::OBSTACLE && std::find(known.begin(), known.end(), lower) == known.end())
    {
      frontier.push_back(lower);
    }
  }
  if ( cell-> y < world->cells.size() - 1)
  {
    Cell* upper = world->cells[cell->y + 1][cell->x];
    if (upper->cell_status != Cell_status::OBSTACLE && std::find(known.begin(), known.end(), upper) == known.end())
    {
      frontier.push_back(upper);
    }
  }
  if ( cell->x > 0)
  {
    Cell* left = world->cells[cell->y][cell->x - 1];
    if (left->cell_status != Cell_status::OBSTACLE && std::find(known.begin(), known.end(), left) == known.end())
    {
      frontier.push_back(left);
    }
  }
  if ( cell->x < world->cells[0].size() - 1)
  {
    Cell* right = world->cells[cell->y][cell->x + 1];
    if (right->cell_status != Cell_status::OBSTACLE && std::find(known.begin(), known.end(), right) == known.end())
    {
      frontier.push_back(right);
    }
  }
}

float distance_from_unexplored(Cell* cell, World* world)
{
  std::unordered_set<Cell*> known;
  known.insert(cell);
  std::vector<Cell*> frontier;
  add_neighbours(frontier, world, cell, known);
  int idx = 0;
  while (true)
  {
    Cell* current = frontier[idx];
    if (std::find(world->unexplored_cells.begin(), world->unexplored_cells.end(), current) != world->unexplored_cells.end()) return man_dist(cell, current);
    add_neighbours(frontier, world, current, known);
    ++idx;
    if (idx == frontier.size()) return std::numeric_limits<float>::infinity();
  }

}
