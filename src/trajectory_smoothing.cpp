#include <Eigen/Dense>
#include <math.h>

#include <mrs_msgs/TrajectoryReference.h>

Eigen::MatrixXf preprocess_trajectory(mrs_msgs::TrajectoryReference& cmd,int p=9, float cell_size=1)
{
  int m = cmd.points.size();
  // initialize the matrix
  Eigen::MatrixXf preprocessed(p * (m - 1) + 1, 2);
  preprocessed.setZero();

  // fill the matrix
  for (int i = 0; i < m - 1; ++i)
  {
    float dif_x = (float) cmd.points[i+1].position.x - (float) cmd.points[i].position.x;
    float dif_y = (float) cmd.points[i+1].position.y - (float) cmd.points[i].position.y;
    for (int j = 0; j < p; ++j)
    {
      preprocessed(p*i + j, 0) = (float) cmd.points[i].position.x + ( ((float)j /(float) p) * dif_x) + (float) cell_size/2;
      preprocessed(p*i + j, 1) = (float) cmd.points[i].position.y + ( ((float)j /(float) p) * dif_y) + (float) cell_size/2;
    }
  }

  // last point
  preprocessed(p * (m-1),0) = (float) cmd.points[m-1].position.x + (float) cell_size / 2;
  preprocessed(p * (m-1),1) = (float) cmd.points[m-1].position.y + (float) cell_size / 2;

  return preprocessed;
}

float calculate_heading(float v_x, float v_y)
{
  return atan2(v_y, v_x);
}

void smooth_trajectory(mrs_msgs::TrajectoryReference& cmd,float z, int p, float cell_size, float u)
{
  // step 1: preprocess trajectory
  Eigen::MatrixXf preprocessed = preprocess_trajectory(cmd, p, cell_size);
  std::cout << "Trajectory preprocessed." << std::endl;
  //std::cout << preprocessed << std::endl;
  int m = preprocessed.rows();
  int n = preprocessed.cols();

  // step 2: generate the A matrix and vector b
  Eigen::MatrixXf A(2 * (m - 1) + 2*m, 2*m);
  A.setZero();

  Eigen::VectorXf b(2 * (m - 1) + 2*m);
  b.setZero();

  for (int i = 0; i < m-2; ++i)
  {
    // equation for x
    if (i%p != 0) A(2*i, i) = (float) 1 / 3;
    if ((i+1)%p != 0) A(2*i, i+1) = (float) -2 / 3;
    if ((i+2)%p != 0) A(2*i, i+2) = (float) 1 / 3;

    // equation for y
    if (i%p != 0) A(2*i+1, m+i) = (float) 1 / 3;
    if ((i+1)%p != 0) A(2*i+1, m+i+1) = (float) -2 / 3;
    if ((i+2)%p != 0) A(2*i+1, m+i+2) = (float) 1 / 3;

    // vector b
    b(2*i) = ((float) 2/3) * preprocessed(i+1, 0) - ((float) 1/3) * preprocessed(i, 0) - ((float) 1/3) * preprocessed(i+2, 0);
    b(2*i+1) = ((float) 2/3) * preprocessed(i+1, 1) - ((float) 1/3) * preprocessed(i, 1) - ((float) 1/3) * preprocessed(i+2, 1);
  }

  // dont change first point
  //A(0, 0) = 0;
  //A(1, m) = 0;

  // dont change last point
  //A(2*(m-2) - 2, m -1) = 0;
  //A(2*(m-2) - 1, 2*m -1) = 0;
  
  // smooth connection of end and start
  A(2*(m-2), 1) = (float) 1/3;
  A(2*(m-2), m-2) = (float) 1/3;
  A(2*(m-2) + 1, m + 1) = (float) 1/3;
  A(2*(m-2) + 1, 2*m - 2) = (float) 1/3;

  b(2*(m-2)) = ((float)2/3) * preprocessed(0,0) - ((float) 1/3)*preprocessed(1,0) - ((float) 1/3) * preprocessed(m-2,0);
  b(2*(m-2)+1) = ((float)2/3) * preprocessed(0,1) - ((float) 1/3)*preprocessed(1,1) - ((float) 1/3) * preprocessed(m-2,1);

  // penalize big changes
  A.bottomRows(2*m) = Eigen::MatrixXf::Identity(2*m, 2*m) * u;

  // step 3: calculate the changes
  std::cout << "Matrices generated." << std::endl;
  //Eigen::VectorXf solution = A.colPivHouseholderQr().solve(b); // better solution but slower
  Eigen::VectorXf solution = (A.transpose() * A).ldlt().solve(A.transpose() * b);
  std::cout << "Solution calculated." << std::endl;
  // step 4: update the trajectory
  //std::cout << "A is" << std::endl << A << std::endl;
  //std::cout << "b is" << std::endl << b << std::endl;
  //std::cout << "solution is " << solution << std::endl;
  bool big_turn = false;
  cmd.points.clear();
  for (int i = 0; i < preprocessed.rows(); ++i)
  {
    mrs_msgs::Reference point;
    point.heading = 0; 
    if (i%p == 0) // dont change the points that should not be changed
    {
      //std::cout << "Zeroing " << solution(i,0) << ", " << solution(m+i, 0) << std::endl;
      solution(i,0) = 0;
      solution(m+i, 0) = 0;
    }
    point.position.x = preprocessed(i, 0) + solution(i, 0);
    point.position.y = preprocessed(i, 1) + solution(m+i, 0);
    point.position.z = z;
    if (i > 0)
    {
      float last_heading = calculate_heading((float) point.position.x - (float) cmd.points.back().position.x, (float) point.position.y - (float) cmd.points.back().position.y);
      if (abs(last_heading - cmd.points[cmd.points.size() - 2].heading) > 3.15) big_turn = true; // too big turn
      cmd.points.back().heading = last_heading;
    }
    cmd.points.push_back(point);
  }
  cmd.dt = cmd.dt / p;
  // last heading
  cmd.points.back().heading = cmd.points.front().heading;

  std::cout << "Converted to trajectory." << std::endl;
  //solve the issues with big turns
  for (int i = 2*p; i < cmd.points.size(); i = i + p)
  {
    if (cmd.points[i].position.x == cmd.points[i-2*p].position.x && cmd.points[i].position.y == cmd.points[i-2*p].position.y) // a 180 degree turn
    {
      float base = cmd.points[i - p].heading;
      if (base > 0) base = base - (float) M_PI;
      else base = base + (float) M_PI;
      float final_angle = cmd.points[i].heading;
      
      float diff;
      if ((base > 0 && final_angle > 0) || (final_angle < 0 && base < 0)) diff = final_angle - base;
      else
      {
        float diff1;
        float diff2;
        if (final_angle > 0)
        {
          diff1 = base + 2*(float)M_PI - final_angle; 
          diff2 = -final_angle + base;
        }
        else
        {
          diff1 = final_angle + 2*(float)M_PI - base; 
          diff2 = -base + final_angle;
        }
        if (abs(diff1) <= abs(diff2)) diff = diff1;
        else diff = diff2;
      }
      float step = diff/(float)p;
      for (int k = 0; k < p; ++k)
      {
        float cur_heading = base + (float) k*step;
        if (cur_heading > M_PI) cur_heading = cur_heading - (float)M_PI;
        if (cur_heading < -M_PI) cur_heading = cur_heading + (float)M_PI;
        cmd.points[i - p + k].heading = cur_heading;  
      }
    }
  }
  std::cout << "Big turns solved." << std::endl;
}

