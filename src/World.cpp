#include "World.h"

#include <vector>
#include "Cell.h"
#include <iostream>
#include <algorithm>

World::World(int n_c_rows, int n_c_cols, float c_size)
  {
    width = n_c_rows;
    height = n_c_cols;
    cell_size = c_size;
    
    cells.resize(height);
    for (int i = 0; i < height; ++i)
    {
      cells[i].resize(width, NULL);
    }
  }

  World::~World()
  {
    for (int i = 0; i < height; ++i)
    {
      for (int j = 0; j < width; ++j)
      {
        delete cells[i][j];
      }
    }
  }

  void World::create_world_cells(std::vector<std::vector<int>>& world_matrix)
  {
    for (int i = 0; i < height; ++i)
    {
      for (int j = 0; j < width; ++j)
      {
        // convert indexes so that [0, 0] is left lower corner
        int y_idx = height - 1 - i;
        if (world_matrix[y_idx][j] == 1)
        {
          cells[i][j] = new Cell(j , i, Cell_status::OBSTACLE);
        }
        else
        {
          cells[i][j] = new Cell(j , i, Cell_status::UNEXPLORED);
          unexplored_cells.insert(cells[i][j]);
        }
      }
    }
  }


  void World::visualise(Cell* current_pos)
  // a method to visualize the world
  {
    for (int i = height - 1; i >= 0; --i)
    {
      for (int j = 0; j < width; ++j)
      {
        // print 'H' for current position
        if (current_pos->x == j && current_pos->y == i)
        {
          std::cout << 'H' << ' ';
        }
        else
        {
          if  (cells[i][j]->spanning_trees.size() == 0)
          {
            if (cells[i][j]->cell_status == Cell_status::OBSTACLE)
            {
              std::cout << 'X' << ' ';
            }
            else if (cells[i][j]->cell_status == Cell_status::EXPLORED)
            {
              std::cout << 'E' << ' ';
            }
            else
            {
              std::cout << '0' << ' ';
            }
          }
          else
          {
            std::cout << *(cells[i][j]->spanning_trees.begin())  << ' ';
          }
        }
      }
      std::cout << std::endl;
    }
  }

  void World::erase_from_unexplored(Cell* cell)
  {
    std::unordered_set<Cell*>::iterator pos = std::find(unexplored_cells.begin(), unexplored_cells.end(), cell);
    if (pos != unexplored_cells.end()) unexplored_cells.erase(pos);
  }
