#include "Cell.h"
#include <iostream>
#include <algorithm>
Cell::Cell(int X, int Y, Cell_status status)
  {
    x = X;
    y = Y;
    cell_status = status;
    visited = false;
    steps_from_start = 10000000;
  }

  void Cell::add_edge(Cell* cell)
  // add an edge into the edge vector
  {
    edges.push_back(cell);
  }

  void Cell::remove_edge(Cell* cell)
  // remove an edge
  {
    edges.erase(std::remove(edges.begin(), edges.end(), cell), edges.end());
  }

  bool Cell::in_spanning_tree(int id)
  // check whether this cell is in an spanning tree with given id
  {
    return spanning_trees.find(id) != spanning_trees.end();
  }

  Cell* Cell::go_to_next_cell(int start_x, int start_y)
  // choose one edge and go there 
  {
    Cell* start = NULL; // if edge to start is present, store it here
    for (int i = 0; i < edges.size(); ++i)
    {
      if (edges[i]->visited == false)
      {
        //std::cout << "Found a non-visited cell [" << edges[i]->x << ", " << edges[i]->y << "]." << std::endl;
        edges[i]->visited = true;
        return edges[i];
      }
      if (edges[i]->x == start_x && edges[i]->y == start_y)
      {
        start = edges[i];
      }
    }
    if (start != NULL)
    {
      return start;
    }
    float min_dist= edges[0]->steps_from_start;
    Cell* min_element = edges[0];
    for (int i = 0; i < edges.size(); ++i)
    {
      int dist = edges[i]->steps_from_start;
      if (dist < min_dist)
      {
        min_dist = dist;
        min_element = edges[i];
      }
    }
    return min_element;
  }

  void Cell::print_edges()
  // function for debug
  {
    std::cout << "Printing edges of cell [" << x << ", " << y << "]." << std::endl;
    for (auto it: edges)
    {
      std::cout << "Edge to [" << it->x << ", " << it->y << "]." << std::endl;
    }
  }

  bool Cell::all_edges_visited()
  // return true of all edges from this cell were already visited
  {
    for (auto it: edges)
    {
      if (it->visited == false)
      {
        return false;
      }
    }
    return true;
  }

  void Cell::count_steps_from_start()
  {
    for (auto it: edges)
    {
      if (it->steps_from_start < steps_from_start-1)
      {
        steps_from_start = it->steps_from_start+1;
      }
    }
  }

  void Cell::erase_edges()
  {
    edges.clear();
  }
