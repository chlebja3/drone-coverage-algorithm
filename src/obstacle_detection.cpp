#include "Cell.h"
#include "utils.h"

bool update_map_position(Cell*& current_pos, float env_x1, float env_y1, float env_x2, float env_y2, float cell_size,  double pos_x, double pos_y)
{
  if (check_validity((float) pos_x,(float) pos_y, env_x1, env_y1, env_x2, env_y2) == false)
  {
    current_pos->x = -1;
    current_pos->y = -1;
  }
  else
  {
    int new_x = (int) (pos_x - env_x1)/cell_size;
    int new_y = (int) (pos_y - env_y1)/cell_size;
    if (current_pos->x != new_x || current_pos->y != new_y)
    {
      current_pos->x = new_x;
      current_pos->y = new_y;
      return true;
    }
  }
  return false;
}


