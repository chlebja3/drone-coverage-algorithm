#include <math.h>
#include <Spanning_tree.h>
#include <distance.h>
#include <World.h>
#include <iostream>
#include <limits>
#include <ros/ros.h>

#include "Cell.h"
#include "drone_coverage_planner/pos_change.h"
#include "utils.h"

int cell_status_to_int(Cell_status status)
// convert cell status to int as follows
// 0 means UNEXPLORED or OBSTACLE, 1 means EXPLORED
{
  return status == Cell_status::EXPLORED?1:0;
}

void make_edge(Cell* cell1, Cell* cell2)
// create an edge between two cells
{
  cell1->add_edge(cell2);
  cell2->add_edge(cell1);
}

float euc_dist_numbers(int x1, int y1, int x2, int y2)
{
  return (float) pow(x1-x2,2) + (float) pow(y1-y2,2);
}

int man_dist_numbers(int x1, int y1, int x2, int y2)
{
  return abs(x1-x2) + abs(y1-y2);
}

Cell* nearest_cell_from_tree(Spanning_tree& tree, Cell* cell)
{
  float min_dist = euc_dist(tree.cells[0], cell);
  Cell* nearest = tree.cells[0];
  for (int i = 0; i < tree.cells.size(); ++i)
  {
    float dist = euc_dist(tree.cells[i], cell);
    if (dist < min_dist)
    {
      min_dist = dist;
      nearest = tree.cells[i];
    }
  }
  return nearest;
}

std::vector<Cell*> get_candidate_cells(Spanning_tree& tree, World* world)
{
 std::vector<Cell*> candidates;
 for (auto it: tree.cells)
 {
   int x = it->x;
   int y = it->y;
   int tree_id = tree.get_id();
   //upper cell
   if(y > 0)
   {
     Cell* cell = world->cells[y-1][x];
     if (cell->cell_status != Cell_status::OBSTACLE && cell->in_spanning_tree(tree_id) == false)
     {
        candidates.push_back(cell);
        //ROS_INFO("Found candidate [%d, %d]", x, y-1);
     }
   }

   //lower cell
   if(y < world->height - 1)
   {
     Cell* cell = world->cells[y+1][x];
     //if (cell->in_spanning_tree(tree_id) == false)
     if (cell->cell_status != Cell_status::OBSTACLE && cell->in_spanning_tree(tree_id) == false)
     {
        candidates.push_back(cell);
        //ROS_INFO("Found candidate [%d, %d]", x, y+1);
     }
   }

   //left cell
   if(x > 0)
   {
     Cell* cell = world->cells[y][x-1];
     if (cell->cell_status != Cell_status::OBSTACLE && cell->in_spanning_tree(tree_id) == false)
     {
        candidates.push_back(cell);
        //ROS_INFO("Found candidate [%d, %d]", x-1, y);
     }
   }

   //right cell
   if(x < world->width- 1)
   {
     Cell* cell = world->cells[y][x+1];
     if (cell->cell_status != Cell_status::OBSTACLE && cell->in_spanning_tree(tree_id) == false)
     {
        candidates.push_back(cell);
        //ROS_INFO("Found candidate [%d, %d]", x+1, y);
     }

    }
  }
 return candidates;
}

float calculate_cell_score(const float center_x, const float center_y, Cell* target_cell, std::vector<Spanning_tree>& other_trees, Spanning_tree& tree, World* world)
// calculate the score of a given cell
{
  int tree_id = tree.get_id();
  // constants with naming conventions from the paper
  int sigma_a = 1;
  int sigma_r = 100; // for more uavs
  int eta_M = -100000;
  int sigma_b = 10;
  float sigma_d = 0.0001;
  
  //float Ea = (float) sigma_a * euc_dist_to_center(center_x, center_y, target_cell); 
  float Ea = (float) sigma_a * man_dist_to_center(center_x, center_y, target_cell); 
  //float sum_of_Er = 0;
  float Er = 0; 
  float minimal = std::numeric_limits<float>::infinity(); 
  for (int i = 0; i < other_trees.size(); ++i)
  {
    float now = (float) sigma_r*dist_from_tree(other_trees[i], target_cell);
    Er += now;
    if (now < minimal) minimal = now;
  }
  //int Ei =  eta_M*(target_cell->spanning_trees.size()==0?0:1);
  int Ei = 0;
  if (target_cell->cell_status == Cell_status::EXPLORED) Ei = eta_M*distance_from_unexplored(target_cell, world);

  // my term for trajectories with less turn
  float Eb = 0;
  if (target_cell->y > 0)
  {
    if (world->cells[target_cell->y-1][target_cell->x]->in_spanning_tree(tree_id)) ++Eb;
    if (target_cell->x > 0 && world->cells[target_cell->y-1][target_cell->x-1]->in_spanning_tree(tree_id)) ++Eb;
    if (target_cell->x < world->width-1 && world->cells[target_cell->y-1][target_cell->x+1]->in_spanning_tree(tree_id)) ++Eb;
  }

  if (target_cell->y < world->height - 1)
  {
    if (world->cells[target_cell->y + 1][target_cell->x]->in_spanning_tree(tree_id)) ++Eb;
  
    if (target_cell->x > 0 && world->cells[target_cell->y+1][target_cell->x-1]->in_spanning_tree(tree_id)) ++Eb;

    if (target_cell->x < world->width - 1 && world->cells[target_cell->y+1][target_cell->x + 1]->in_spanning_tree(tree_id)) ++Eb;
  }
  if (target_cell->x > 0 && world->cells[target_cell->y][target_cell->x-1]->in_spanning_tree(tree_id)) ++Eb;

  if (target_cell->x < world->width - 1 && world->cells[target_cell->y][target_cell->x + 1]->in_spanning_tree(tree_id)) ++Eb;
  
  Eb = (float) sigma_b * Eb;

  float Ed = sigma_d * (float) pow(minimal, 2);
 
  //std::cout << "Ea:  " << Ea << ", Er: " << Er << ", Ei:" << Ei << ", Eb: " << Eb << std::endl;

  return Ea + Er + (float)Ei + Eb + Ed; 
}

int count_neighbours_in_ST(Cell* cell, Cell*& next_cell, int st_num, World* world )
{
  int i = cell->y;
  int j = cell->x;
  int ret = 0;
  if (i > 0 && world->cells[i-1][j]->in_spanning_tree(st_num))
  {
    ++ret;
    next_cell = world->cells[i-1][j];
  }
  if (i < world->height - 1 && world->cells[i+1][j]->in_spanning_tree(st_num))
  {
    ++ret;
    next_cell = world->cells[i+1][j];
  }
  if (j > 0 && world->cells[i][j-1]->in_spanning_tree(st_num))
  {
    ++ret;
    next_cell = world->cells[i][j-1];
  }
  if (j < world->width - 1 && world->cells[i][j+1]->in_spanning_tree(st_num))
  {
    ++ret;
    next_cell = world->cells[i][j+1];
  }
  return ret;
}

bool can_unmark(Cell* cell, int st_num, World* world)
{
  int i = cell->y;
  int j = cell->x;
  if ((i > 0 && world->cells[i-1][j]->in_spanning_tree(st_num)) && !((j > 0 && world->cells[i-1][j-1]->in_spanning_tree(st_num)) || (j < world->width - 1 && world->cells[i-1][j+1]->in_spanning_tree(st_num)))) return false;
  if ((i < world->height-1 && world->cells[i+1][j]->in_spanning_tree(st_num)) && !((j > 0 && world->cells[i+1][j-1]->in_spanning_tree(st_num)) || (j < world->width - 1 && world->cells[i+1][j+1]->in_spanning_tree(st_num)))) return false;
  if ((j > 0 && world->cells[i][j-1]->in_spanning_tree(st_num)) && !((i > 0 && world->cells[i-1][j-1]->in_spanning_tree(st_num)) || (i < world->height - 1 && world->cells[i+1][j-1]->in_spanning_tree(st_num)))) return false;
  if ((j < world->width-1 && world->cells[i][j+1]->in_spanning_tree(st_num)) && !((i > 0 && world->cells[i-1][j+1]->in_spanning_tree(st_num)) || (i < world->height- 1 && world->cells[i+1][j+1]->in_spanning_tree(st_num)))) return false;
  return true;
}

void pick_cell(Spanning_tree& spanning_tree, World* world, std::vector<Spanning_tree>& other_spanning_trees, std::vector<Cell*>& redundant_cells, Cell*& current_pos)
{
    //std::vector<Cell*> candidate_cells = world->unexplored_cells; //VARIANT1 
    std::vector<Cell*> candidate_cells = get_candidate_cells(spanning_tree, world); //VARIANT 2
    
    //ROS_INFO("Counting cell score of candidate cells.");

    //calculate the score of all reachable cells
    std::vector<Cell*> expl_cand; // already explored candidates
    Cell* best;
    float best_score = -1 * std::numeric_limits<float>::infinity();
    bool found = false;
    for (int i = 0; i < candidate_cells.size(); ++i)
    {
      if (candidate_cells[i]->cell_status == Cell_status::EXPLORED)
      {
        //ROS_INFO("Found explored cell among the candidates");
        expl_cand.push_back(candidate_cells[i]);
        continue;
      }
      //ROS_INFO("Calculating score of unexplored cell");
      float score = calculate_cell_score(world->center_oi_x, world->center_oi_y, candidate_cells[i], other_spanning_trees, spanning_tree, world);
      if (score > best_score)
      {
        best_score = score;
        best = candidate_cells[i];
        found = true;
      }
    }
    //ROS_INFO("Unexplored cells evaluated. Explored candidates number is %ld", expl_cand.size());
    if (!found)
    {
      for (int i = 0; i < expl_cand.size(); ++i)
      {
        float score = calculate_cell_score(world->center_oi_x, world->center_oi_y, expl_cand[i], other_spanning_trees, spanning_tree, world);
        if (score > best_score)
        {
          best_score = score;
          best = expl_cand[i];
          found = true;
        }
      }
      //ROS_INFO("Explored cells evaluated.");
    }

    // explore the cell with biggest score
    current_pos = best;
    //ROS_INFO("Best cell is [%d, %d]", current_pos->x, current_pos->y);
    if (current_pos->cell_status == Cell_status::EXPLORED)
    {
      redundant_cells.push_back(current_pos);
    }
    else current_pos->cell_status = Cell_status::EXPLORED;
    current_pos->spanning_trees.insert(spanning_tree.get_id());
    world->erase_from_unexplored(current_pos);
    //ROS_INFO("Chose cell [%d, %d], with loss %f.", current_pos->x, current_pos->y, calculate_cell_score(world->center_oi_x, world->center_oi_y, candidate_cells[max_element_index], other_spanning_trees, spanning_tree, world));
    
    ROS_INFO("Explored %d / %d cells.", world->height*world->width - (int) world->unexplored_cells.size(), world->height*world->width);

    /* 
    // add the edge to the spanning tree
    Cell* nearest = nearest_cell_from_tree(&spanning_tree, current_pos);
    */

    spanning_tree.cells.push_back(current_pos);
    //ROS_INFO("Nearest cell from the spanning tree is [%d, %d].", nearest->x, nearest->y);

    // add edge
    //make_edge(nearest, current_pos);

    //visualise the progress
    //world->visualise(current_pos);

    //debug only
    //usleep(100000);
}
